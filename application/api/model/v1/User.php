<?php


namespace app\api\model\v1;
use app\api\model\BaseModel;
use think\Db;
use think\Request;
class User extends BaseModel
{
    public static function  UserInfo(){
        $request = Request::instance();
        $loginid = $request->post('loginid');
        $where["loginid"]=$loginid;
        $list=Db::table('cms_user')
                ->fetchSql(false)
                ->cache(true)
                ->where($where)
                ->field('id,username,headimgurl,country,province,city,sex,usercode,sharecode,loginid')
                ->find();
        return $list;
    }

}