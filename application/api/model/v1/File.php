<?php


namespace app\api\model\v1;
use app\api\model\BaseModel;
use think\Db;
use think\Request;
class File extends BaseModel
{
    public static function  FileList(){
        $request = Request::instance();
        $pageNumberSize = $request->post('pageNumberSize') ? $request->post('pageNumberSize') : 20;
        $pageNumber = $request->post('pageNumber') ? $request->post('pageNumber') : 1;
        $keyword = $request->post('keyword');
        $start = $request->post('start');
        $end = $request->post('end');
        $skip     = ($pageNumber-1)*$pageNumberSize>0 ? ($pageNumber-1)*$pageNumberSize : 0;
        $where=array();

        $keyword=trim($keyword);
        if($keyword){
            $where['file_name']=array('like','%'.$keyword.'%');
        }

        if(!empty($start)&&!empty($end)){
            $strstart =strtotime($start);
            $strend = strtotime($end);
            $where['createdate']=array(array('egt',$start),array('elt',$end));
        }

        $list=Db::table('cms_file')->fetchSql(false)->where($where)->order(array('createdate'=>'desc'))->limit($skip,$pageNumberSize)->field('id,file_url')->select();
        $data['count']=Db::table('cms_file')->fetchSql(false)->where($where)->count();
        $data['pageNumberSize']=$pageNumberSize;
        $data['list']=$list;
        return $data;
    }


}