<?php


namespace app\api\model\v1;
use app\api\model\BaseModel;
use think\Db;
use think\Request;
class Login extends BaseModel
{
    /*
        账号密码登录
     */
    public static function  GetLogin(){
        $request = Request::instance();
        $loginid = $request->post('loginid');
        $loginpwd = $request->post('loginpwd');
        $info =Db::table('cms_user')->where('loginid="' . $loginid . '"')->find();
        $error="账号密码错误!";
        if ($info !== null) {
            if ($info['loginpwd'] === '123456') {
                $data = array(
                    'lastlogin'     => date("Y-m-d H:i:s",time()),
                    'access_token'  => md5($info['id'].time().rand(1000,9999)),
                    'refresh_token' => md5($info['id'].time().rand(1000,9999)),
                    'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+7200)
                );
                Db::table('cms_user')->where("id=".$info['id'])->update($data);
                return $data;
                exit;

            } else {

                return $error;
            }

        } else {
            return $error;
        }

    }
    /*
        刷新refreshtoken
     */
    public static function GetRefreshLogin(){
        $request = Request::instance();
        $refresh_token = $request->post('refresh_token');
        $info =Db::table('cms_user')->where('refresh_token="' . $refresh_token . '"')->find();
        $error="refresh_token出现异常!";
        if ($info) {
                $data = array(
                    'lastlogin'     => date("Y-m-d H:i:s",time()),
                    'access_token'  => md5($info['id'].time().rand(1000,9999)),
                    'refresh_token' => md5($info['id'].time().rand(1000,9999)),
                    'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+7200),
                    'loginid'       => $info['loginid']
                );
                Db::table('cms_user')->where("id=".$info['id'])->update($data);
                return $data;
                exit;
        } else {
            return $error;
        }
    }

    /*
        微信授权登录
     */
    public static function WxOauthLogin(){
        $request = Request::instance();
        $loginid = $request->post('openid');
        $username = $request->post('nickName');
        $headimgurl = $request->post('avatarUrl');
        $country = $request->post('country');
        $province = $request->post('province');
        $city = $request->post('city');
        $sex = $request->post('sex');
        $data = array(
            'loginid'           =>$loginid,
            'username'          =>$username ? $username : '',
            'headimgurl'        =>$headimgurl  ? $headimgurl : '',
            'country'           =>$country   ? $country : '',
            'province'          =>$province   ? $province : '',
            'city'              =>$city   ? $city : '',
            'sex'               =>$sex   ? $sex : '',
            'lastlogin'     => date("Y-m-d H:i:s",time()),
            'access_token'  => md5($info['id'].time().rand(1000,9999)),
            'refresh_token' => md5($info['id'].time().rand(1000,9999)),
            'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+7200)
         );
        $info =Db::table('cms_user')->where('loginid="' . $loginid . '"')->find();
        if ($info !== null) {
        		$data['editdate']=date('Y-m-d H:i:s');
            	$data['editor']=$username ? $username : '';
        		$data1=unset_empty_array($data);
                Db::table('cms_user')
                	->where("id=".$info['id'])
                	->update($data1);
				$userlist=Db::table('cms_user')
							->where("id=".$info['id'])
							->field('lastlogin,access_token,refresh_token,overdue_date,loginid')
							->find();
                return $userlist;
        } else {
            $data['usercode']=randusercode();
            $data['sharecode']=randshare();
            $data['createdate']=date('Y-m-d H:i:s');
            $data['creator']=$username ? $username : '';
            $data['editdate']=date('Y-m-d H:i:s');
            $data['editor']=$username ? $username : '';
            $data1=unset_empty_array($data);
            $id=Db::table('cms_user')
            		->insertGetId($data1);
            $userlist=Db::table('cms_user')
            			   ->where("id=".$id)
            			   ->field('lastlogin,access_token,refresh_token,overdue_date,loginid')
            			   ->find();
            return $userlist;
        }
    }
}