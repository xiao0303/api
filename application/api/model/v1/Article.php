<?php


namespace app\api\model\v1;
use app\api\model\BaseModel;
use think\Db;
use think\Request;
class Article extends BaseModel
{
    public static function  ArticleList(){
        $request = Request::instance();
        $pageSize = $request->post('pageSize') ? $request->post('pageSize') : 20;
        $pageNumber = $request->post('pageNumber') ? $request->post('pageNumber') : 1;
        $keyword = $request->post('keyword');
        $type = $request->post('type');
        $start = $request->post('start');
        $end = $request->post('end');

        $skip     = ($pageNumber)*$pageSize>0 ? ($pageNumber-1)*$pageSize : 0;
        $keyword=trim($keyword);
        $type=trim($type);

        if($keyword){
            $where['chapter_name|message']=array('like','%'.$keyword.'%');
        }
        if(!empty($type)&&isset($type)){
            $where['type']=$type;
        }
        if(!empty($start)&&!empty($end)){
            $strstart =strtotime($start);
            $strend = strtotime($end);
            $where['createdate']=array(array('egt',$start),array('elt',$end));
        }

        		$list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->order(array('id'=>'asc'))->limit($skip,$pageSize)->select();
        // echo $list;
        $data['count']=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->count();
        $data['pageSize']=$pageSize;
        $data['list']=$list;
        return $data;
    }

    public  static function  ArticleAdd(){
        $request = Request::instance();
        p($request);
    }
}