<?php


namespace app\api\controller\v1;
use app\api\controller\BaseController;
use app\api\model\v1\Detail as DetailModel;
use app\api\validate\v1\Detail as DetailValidate;
use think\Request;
class Detail extends BaseController
{
    //  http://api.com/index.php/api/v1/ArticleDetail/index
    public function index(){
        echo '123';
    }
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/ArticleDetail/GetArticleDetail
     */
    public function GetArticleDetail()
    {
        // 输入数据验证
        $validate = new DetailValidate();
        $validate->goCheck('detail');
        // 返回数据
        $list=DetailModel::DetailInfo();
        $this->show_success($list);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/ArticleDetail/GetDetailLeft
     */
    public function GetDetailLeft(){
        // 输入数据验证
        $validate = new DetailValidate();
        $validate->goCheck('detailLeft');
        // 返回数据
        $list=DetailModel::DetailLeft();
        $this->show_success($list);
    }

    public function GetDetailRight(){
        // 输入数据验证
        $validate = new DetailValidate();
        $validate->goCheck('detailRight');
        // 返回数据
        $list=DetailModel::DetailRight();
        $this->show_success($list);
    }

}