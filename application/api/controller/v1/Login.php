<?php


namespace app\api\controller\v1;
use app\api\controller\BaseController;
use app\api\model\v1\Login as LoginModel;
use app\api\validate\v1\Login as LoginValidate;

class Login extends BaseController
{
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/Login/oauth
     */

    public function oauth()
    {
        // 输入数据验证
        $validate = new LoginValidate();
        $validate->LoginCheck('Login');
        // 返回数据
        $result=LoginModel::GetLogin();
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/Login/oauth
     */

    public function RefreshOauth()
    {

        // 输入数据验证
        $validate = new LoginValidate();
        $validate->LoginCheck('Refresh');
        // 返回数据
        $result=LoginModel::GetRefreshLogin();
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/Login/oauth
     */

    public function WxOauthLogin()
    {
        // 输入数据验证
        $validate = new LoginValidate();
        $validate->LoginCheck('WxOauthLogin');
        // 返回数据
        $result=LoginModel::WxOauthLogin();
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/Login/index
     */
    public function index(){
        $data['usercode']=randusercode();
        $data['sharecode']=randshare();
        dump($data);
    }
}
