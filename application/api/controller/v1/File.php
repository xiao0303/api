<?php


namespace app\api\controller\v1;
use app\api\controller\BaseController;
use app\api\model\v1\File as FileModel;
use app\api\validate\v1\File as FileValidate;

class File extends BaseController
{
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     */
    public function GetFileList()
    {
        // 输入数据验证
        $validate = new FileValidate();
        $validate->goCheck('filelist');
        // 返回数据
        $list=FileModel::FileList();
        $this->show_success($list);
    }
}