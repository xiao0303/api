<?php


namespace app\api\controller\v1;
use app\api\controller\BaseController;
use app\api\model\v1\Article as ArticleModel;
use app\api\validate\v1\Article as ArticleValidate;
use think\Request;
class Article extends BaseController
{
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/Article/GetArticleList
     */
    public function GetArticleList()
    {
        // 输入数据验证
        $validate = new ArticleValidate();
//      $validate->goCheck('articlelist');
        // 返回数据
        $list=ArticleModel::ArticleList();
        $this->show_success($list);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     */
    public function GetArticleAdd()
    {
        // 输入数据验证
        $validate = new ArticleValidate();
        $validate->goCheck('articleadd');
        // 返回数据
        $list=ArticleModel::ArticleAdd();
        $this->show_success($list);
    }

    public function GetLogin(){
        $request = Request::instance();
        $_SESSION['user']['loginid']=$request->post('loginid');
        $_SESSION['user']['password']=$request->post('password');
        $this->show_success($_SESSION['user']);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     *
     */
    public function GetLoginInfo(){
        $str='123456';
        $password=$this->encrypt($str);
        dump($password);
        $str2='VTqozu0Tqm7CMtzt32kJ5g==';
        $aaa=$this->decrypt($str2);
        dump($aaa);
    }



    /*
     * 生成token函数
     * @return [json] [description]
     */
    public function getToken(){
        $request = \think\Request::instance();
        echo  $request->token();
    }


    /**
     * 加密方法
     * @param string $str
     * @return string
     */
    public function encrypt($str){
        //AES, 128 ECB模式加密数据
        $screct_key = md5("123456");
        $screct_key = base64_decode($screct_key);
        $str = trim($str);
        $str = $this->addPKCS7Padding($str);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_ECB),MCRYPT_RAND);
        $encrypt_str =  mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $screct_key, $str, MCRYPT_MODE_ECB, $iv);
        return base64_encode($encrypt_str);
    }

    /**
     * 解密方法
     * @param string $str
     * @return string
     */
    public function decrypt($str){
        //AES, 128 ECB模式加密数据
        $screct_key = md5("123456");
        $str = base64_decode($str);
        $screct_key = base64_decode($screct_key);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_ECB),MCRYPT_RAND);
        $encrypt_str =  mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $screct_key, $str, MCRYPT_MODE_ECB, $iv);
        $encrypt_str = trim($encrypt_str);
        $encrypt_str = $this->stripPKSC7Padding($encrypt_str);
        return $encrypt_str;
    }

    /**
     * 填充算法
     * @param string $source
     * @return string
     */
    private function addPKCS7Padding($source){
        $source = trim($source);
        $block = mcrypt_get_block_size('rijndael-128', 'ecb');
        $pad = $block - (strlen($source) % $block);
        if ($pad <= $block) {
            $char = chr($pad);
            $source .= str_repeat($char, $pad);
        }
        return $source;
    }
    /**
     * 移去填充算法
     * @param string $source
     * @return string
     */
    private function stripPKSC7Padding($source){
        $source = trim($source);
        $char = substr($source, -1);
        $num = ord($char);
        if($num==62)return $source;
        $source = substr($source,0,-$num);
        return $source;
    }
}