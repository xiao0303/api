<?php


namespace app\api\controller\v1;
use app\api\controller\BaseController;
use app\api\model\v1\User as UserModel;
use app\api\validate\v1\User as UserValidate;
use think\Request;
class User extends BaseController
{

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/api/v1/User/GetUserInfo
     */
    public function GetUserInfo(){
        // 输入数据验证
        $validate = new UserValidate();
        $validate->goCheck('User');
        // 返回数据
        $list=UserModel::UserInfo();
        $this->show_success($list);
    }


    /*
     * 生成token函数
     * @return [json] [description]
     */
    public function getToken(){
        $request = \think\Request::instance();
        echo  $request->token();
    }



}