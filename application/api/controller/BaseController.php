<?php


namespace app\api\controller;
use think\Controller;

class BaseController extends Controller
{

    /**
     * 显示错误，并中断程序执行
     * @param string $des 错误描述
     */
    public static function show_error($msg,$code=300) {
        $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        if(in_array($origin, config('domain'))){
            header("Access-Control-Allow-Origin: ".$origin);
            header("Access-Control-Allow-Methods: GET, POST");
            header("Access-Control-Allow-Headers:Authorization,DNT,OPTIONS,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type, Accept-Language, Origin, Accept-Encoding,token,sign,timestamp");
            header("Content-Type: text/html; charset=utf-8");
        }

        $arr=array(
            'status'    =>      'error',
            'data'       =>      $msg,
            'code'      =>       $code
        );
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        exit();
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     */
    public static function show_success($data,$code=200) {
        $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        if(in_array($origin, config('domain'))){
            header("Access-Control-Allow-Origin: ".$origin);
            header("Access-Control-Allow-Methods: GET, POST");
            header("Access-Control-Allow-Headers:Authorization,OPTIONS,DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type, Accept-Language, Origin, Accept-Encoding,token,sign,timestamp");
            header("Content-Type: text/html; charset=utf-8");

        }
        $arr=array(
            'status'    =>      'ok',
            'data'       =>      $data,
            'code'      =>       $code
            );
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
        exit();
    }

}