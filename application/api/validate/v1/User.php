<?php
namespace app\api\validate\v1;
use app\api\validate\BaseValidate;
class User extends BaseValidate
{
    //列表参数验证
    protected  $User=array(
            'sn'        => 'require',
            'loginid'   => 'require',

    );

    //错误语句提示
    protected $message  =   [
        'sn.require'                => 'sn不能为空!',
        'loginid.require'           => 'loginid不能为空!',

    ];

}