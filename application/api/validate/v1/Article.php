<?php
namespace app\api\validate\v1;
use app\api\validate\BaseValidate;
class Article extends BaseValidate
{
    //列表参数验证
    protected   $articlelist=array(
            'sn'        => 'require',
            'pagesize'  => 'integer',
            'page'      => 'integer',
            'start'     => 'date',
            'end'       => 'date'
    );

    //错误语句提示
    protected $message  =   [
        'sn.require'                => 'SN不能为空!',
        'pagesize.integer'          => '每页条数需要填写整数!',
        'page.integer'              => '页码必须为整数!',
        'start.date'                => '开始时间请填写时间格式!',
        'end.date'                  => '结束时间请填写时间格式',
    ];

    //添加参数验证
    protected   $articleadd=array(
            'type'          =>  'require',
            'creator'       =>  'require',
            'createdate'    =>  'createdate',
            'novel_name'    =>  'novel_name',
            'original_url'  =>  'url',
            'chapter_name'  =>  'require',
            'message'       =>  'require'
    );
}