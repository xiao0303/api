<?php
namespace app\api\validate\v1;
use app\api\validate\BaseValidate;
class Detail extends BaseValidate
{
    //列表参数验证
    protected   $detail=array(
            'sn'        => 'require',
            'id'        => 'require|integer',

    );

    //错误语句提示
    protected $message  =   [
        'sn.require'                => 'SN不能为空!',
        'id.require'                => 'id不能为空!',
        'id.integer'                => '页码必须为整数!',

    ];

    //添加参数验证
    protected   $articleadd=array(
            'id'          =>  'require|integer',

    );

    protected $detailLeft=array(
            'sn'        => 'require',
            'id'        => 'require|integer',
        );

    protected $detailRight=array(
            'sn'        => 'require',
            'id'        => 'require|integer',
        );
}
