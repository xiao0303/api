<?php
namespace app\api\validate\v1;
use app\api\validate\BaseValidate;
class Login extends BaseValidate
{
    //列表参数验证
    protected   $Login=array(
            'loginid'   => 'require',
            'loginpwd'  => 'require',
    );

    //错误语句提示
    protected $message  =   [
        'loginid.require'           => '账号不能为空!',
        'loginpwd.integer'          => '密码不能为空!',
        'refresh_token'             => 'refresh_token 不能为空!',
    ];

    protected $Refresh=array(
        'refresh_token'   => 'require',
        );

    protected $WxOauthLogin=array(
        'openid'          => 'require',
        );
}