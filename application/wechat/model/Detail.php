<?php


namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Detail extends BaseModel
{
    public static function  DetailInfo(){
        $request = Request::instance();
        $id = $request->post('id');
        $where['id']=$id;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->find();
        $data['list']=$list;
        return $data;
    }

    public static function DetailLeft(){
        $request = Request::instance();
        $id = $request->post('id');
        $type=$request->post('type');
        $where['id']=array('lt',$id);
        $where['type']=$type;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->order(array('id'=>'desc'))->limit(1)->select();

        $data['list']=$list[0];
        return $data;
    }

    public static function DetailRight(){
        $request = Request::instance();
        $id = $request->post('id');
        $type=$request->post('type');
        $where['id']=array('gt',$id);
        $where['type']=$type;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->order(array('id'=>'asc'))->limit(1)->select();
        $data['list']=$list[0];
        return $data;
    }

}