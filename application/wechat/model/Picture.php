<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\File;
use think\Request;

class Picture extends BaseModel
{
    /**
     * 上传图片
     * @param
     * @return array
     */
    public function InsertOnePicture(){
		$result=$this->uploadOneFile('file','pic',0);
		return $result;
    }

	public function InsertAllPicture(){
		$result=$this->uploadAllFile('file','Picture',1);
		return $result;
	}
}