<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use app\wechat\model\Token;
use think\Db;
use think\Request;
class Project extends BaseModel
{
	/**
     * 列表
     * @param
     * @return array
     */
    public function ProjectList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$userid  =	(new Token())->GetUserid();
		$pageSize = $request->post('pageSize') ? $request->post('pageSize') : 20;
		$pageNumber = $request->post('pageNumber') ? $request->post('pageNumber') : 1;
		$skip     = ($pageNumber)*$pageSize>0 ? ($pageNumber-1)*$pageSize : 0;

        $where['a.userid']=$userid;
        $field=array(
        	'a.id'=>'id',
        	'a.projectname'=>'projectname',
        	'a.startdate'=>'startdate',
        	'a.enddate'=>'enddate',
        	'a.team'=>'team',
        	'a.describe'=>'describe',
            'a.duties'=>'duties',
        	'a.tag'=>'tag',
        	'a.userid'=>'userid',
            'b.companyname'=>'companyname'
        );
		$list=  Db::table('cms_project')
				->alias('a')
				->join('cms_company b',' a.companyid = b.id')
				->field($field)
				->where($where)
				->fetchSql(false)
				->cache(false)
				->order(array('startdate'=>'desc'))
				->limit($skip,$pageSize)
				->select();
		foreach($list as $key =>$value){
			$list[$key]['tag']=explode(',',$value['tag']);
		}
		$data['count']=Db::table('cms_project')
				->alias('a')
				->join('cms_company b',' a.companyid = b.id')
				->field($field)
				->where($where)
				->count();
		$data['pageSize']=$pageSize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function InsertProject(){
    	$request 	= 	Request::instance();
        $projectname 	= 	$request->post('projectname');
        $startdate 		= 	$request->post('startdate');
        $enddate 		= 	$request->post('enddate');
        $team 			= 	$request->post('team');
        $companyid 		= 	$request->post('companyid');
        $describe 		= 	$request->post('describe');
        $duties 		= 	$request->post('duties');
        $tag			=	$request->post('tag');
        $userid 		=	$request->post('userid');
        $data = array(
            'projectname'       =>$projectname,
            'startdate'         =>$startdate,
            'enddate'        	=>$enddate,
            'team'          	=>$team,
            'companyid'         =>$companyid,
            'describe'          =>$describe,
            'duties'          	=>$duties,
            'tag'     			=>$tag,
            'userid'  			=>$userid,
         );
        $id=Db::table('cms_project')->insertGetId($data);
        $info=Db::table('cms_project')->where("id=".$id)->find();
        return $info;
    }
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function UpdateProject(){
    	$request 	= 	Request::instance();
    	$id			=	$request->post('id');
        $projectname 	= 	$request->post('projectname');
        $startdate 		= 	$request->post('startdate');
        $enddate 		= 	$request->post('enddate');
        $team 			= 	$request->post('team');
        $companyid 		= 	$request->post('companyid');
        $describe 		= 	$request->post('describe');
        $duties 		= 	$request->post('duties');
        $tag			=	$request->post('tag');
        $userid 		=	$request->post('userid');
        $data = array(
            'projectname'       =>$projectname,
            'startdate'         =>$startdate,
            'enddate'        	=>$enddate,
            'team'          	=>$team,
            'companyid'         =>$companyid,
            'describe'          =>$describe,
            'duties'          	=>$duties,
            'tag'     			=>$tag,
            'userid'  			=>$userid,
         );
        Db::table('cms_project')->where("id=".$id)->update($data);
        $info=Db::table('cms_project')->where("id=".$id)->find();
        return $info;
    }
    
    public static function DeleteProject(){
		$request 	= 	Request::instance();
		$id			=	$request->post('id');
        $result=Db::table('cms_project')->fetchSql(false)->where("id=".$id)->delete();
        return $result;
	}
}