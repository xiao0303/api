<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Address extends BaseModel
{	
	public static function GetAddressList(){
		$request = Request::instance();
        $pageSize = $request->post('pageSize') ? $request->post('pageSize') : 20;
        $pageNumber = $request->post('pageNumber') ? $request->post('pageNumber') : 1;
        $skip     = ($pageNumber)*$pageSize>0 ? ($pageNumber-1)*$pageSize : 0;
        $keyword = $request->post('keyword');
        $keyword=trim($keyword);
        if($keyword){
            $where['username|telphone|province|city|address']=array('like','%'.$keyword.'%');
        }
        $list=Db::table('cms_user_address')
        		->fetchSql(false)
        		->cache(false)
        		->where($where)
        		->order(array('id'=>'asc'))
        		->limit($skip,$pageSize)
        		->select();
        $data['count']=Db::table('cms_user_address')->fetchSql(false)->cache(false)->where($where)->count();
        $data['pageSize']=$pageSize;
        $data['list']=$list;
		$data['pageNumber']=$pageNumber;
        return $data;
	}
	
	public static function InsertAddress(){
		$request 	= 	Request::instance();
        $userid 	= 	$request->post('userid');
        $sex 		= 	$request->post('sex');
        $username 	= 	$request->post('username');
        $telphone 	= 	$request->post('telphone');
        $province 	= 	$request->post('province');
        $city 		= 	$request->post('city');
        $district 	= 	$request->post('district');
        $address	=	$request->post('address');
        $is_default =	$request->post('is_default');
        $data = array(
            'userid'           	=>$userid,
            'sex'          		=>$sex,
            'username'        	=>$username ? $username : '',
            'telphone'          =>$telphone,
            'province'          =>$province,
            'city'              =>$city ? $city : '',
            'district'          =>$district,
            'address'     		=> $address,
            'is_default'  		=> $is_default ? $is_default : '',
         );
        $id=Db::table('cms_user_address')->insertGetId($data);
        $info=Db::table('cms_user_address')->where("id=".$id)->find();
        return $info;
	}
	
	public static function UpdateAddress(){
		$request 	= 	Request::instance();
		$id			=	$request->post('id');
        $userid 	= 	$request->post('userid');
        $sex 		= 	$request->post('sex');
        $username 	= 	$request->post('username');
        $telphone 	= 	$request->post('telphone');
        $province 	= 	$request->post('province');
        $city 		= 	$request->post('city');
        $district 	= 	$request->post('district');
        $address	=	$request->post('address');
        $is_default =	$request->post('is_default');
        $data = array(
            'userid'           	=>$userid,
            'sex'          		=>$sex,
            'username'        	=>$username ? $username : '',
            'telphone'          =>$telphone,
            'province'          =>$province,
            'city'              =>$city ? $city : '',
            'district'          =>$district,
            'address'     		=> $address,
            'is_default'  		=> $is_default ? $is_default : '',
         );
        Db::table('cms_user_address')->where("id=".$id)->update($data);
        $info=Db::table('cms_user_address')->where("id=".$id)->find();
        return $info;
	}
	
	public static function DeleteAddress(){
		$request 	= 	Request::instance();
		$id			=	$request->post('id');
        $result=Db::table('cms_user_address')->fetchSql(false)->where("id=".$id)->delete();
        return $result;
	}
}