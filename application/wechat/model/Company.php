<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use app\wechat\model\Token;
use think\Db;
use think\Request;
class Company extends BaseModel
{
	/**
     * 获取权限
     * @param
     * @return array
     */
    public function CompanyList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$userid  =	(new Token())->GetUserid();
      	$pagesize = $request->post('pagesize') ? $request->post('pagesize') : 20;
		$page     = $request->post('page') ? $request->post('page') : 0;
		$skip     = ($page-1)*$pagesize>0 ? ($page-1)*$pagesize : 0;
        if($userid){
            $where['userid']=$userid;
        }
		$list=Db::table('cms_company')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->order(array('startdate'=>'desc'))
				->limit($skip,$pagesize)
				->select();
		$data['count']=Db::table('cms_company')->where($where)->count();
		$data['pagesize']=$pagesize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }
    
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function InsertCompany(){
    	$request 	= 	Request::instance();
        $companyname 	= 	$request->post('companyname');
        $startdate 		= 	$request->post('startdate');
        $enddate 		= 	$request->post('enddate');
        $position		= 	$request->post('position');
        $industry 		= 	$request->post('industry');
        $total  		= 	$request->post('total');
        $department 	= 	$request->post('department');
        $nature 		=	$request->post('nature');
        $workdescribe	=   $request->post('workdescribe');
        $isfulltime		=   $request->post('isfulltime');
        $dimissionreason=   $request->post('dimissionreason');
        $important		=   $request->post('important');
        $userid 		=	$request->post('userid');
        $data = array(
            'companyname'       =>$companyname,
            'startdate'         =>$startdate,
            'enddate'        	=>$enddate,
            'position'          =>$position,
            'industry'         	=>$industry,
            'total'          	=>$total,
            'department'        =>$department,
            'nature'     		=>$nature,
            'workdescribe'     	=>$workdescribe,
            'isfulltime'     	=>$isfulltime,
            'dimissionreason'   =>$dimissionreason,
            'important'     	=>$important,
            'userid'  			=>$userid,
         );
        $id=Db::table('cms_company')->insertGetId($data);
        $info=Db::table('cms_company')->where("id=".$id)->find();
        return $info;
    }
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function UpdateCompany(){
    	$request 	= 	Request::instance();
    	$id			=	$request->post('id');
        $companyname 	= 	$request->post('companyname');
        $startdate 		= 	$request->post('startdate');
        $enddate 		= 	$request->post('enddate');
        $position		= 	$request->post('position');
        $industry 		= 	$request->post('industry');
        $total  		= 	$request->post('total');
        $department 	= 	$request->post('department');
        $nature 		=	$request->post('nature');
        $workdescribe	=   $request->post('workdescribe');
        $isfulltime		=   $request->post('isfulltime');
        $dimissionreason=   $request->post('dimissionreason');
        $important		=   $request->post('important');
        $userid 		=	$request->post('userid');
        $data = array(
            'companyname'       =>$companyname,
            'startdate'         =>$startdate,
            'enddate'        	=>$enddate,
            'position'          =>$position,
            'industry'         	=>$industry,
            'total'          	=>$total,
            'department'        =>$department,
            'nature'     		=>$nature,
            'workdescribe'     	=>$workdescribe,
            'isfulltime'     	=>$isfulltime,
            'dimissionreason'   =>$dimissionreason,
            'important'     	=>$important,
            'userid'  			=>$userid,
         );
        Db::table('cms_project')->where("id=".$id)->update($data);
        $info=Db::table('cms_company')->where("id=".$id)->find();
        return $info;
    }
    
    public static function DeleteCompany(){
		$request 	= 	Request::instance();
		$id			=	$request->post('id');
        $result=Db::table('cms_company')->fetchSql(false)->where("id=".$id)->delete();
        return $result;
	}
}