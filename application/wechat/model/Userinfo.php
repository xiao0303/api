<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use app\wechat\model\Token;
use think\Db;
use think\Request;
class Userinfo extends BaseModel
{
	/**
     *
     * @param
     * @return array
     */
    public function Information(){
      	$request = Request::instance();
      	$token = $request->post('token');
      	$where['id']=(new Token())->GetUserid();
		$list=Db::table('cms_user')
				->field('id as userid,sex,headimgurl,realname,country,
				province,city,headimgurl,email,qq,signature,mobilephone,
				lastlogin,college,datebirth,workyears,intention,specialty,record,skills,evaluation')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->find();
       	return $list;
    }

    /**
     *
     * @param
     * @return array
     */
    public function InfoQuotations(){
      	$request = Request::instance();
      	$userid = (new Token())->GetUserid();
      	$where['userid']=$userid;
      	$where['cms_key']=array('in','project,experience,example');
		    $list=Db::table('cms_value')
				->field('cms_key,cms_value,creator,createdate,editor,editdate')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->select();
    		$items=array();
    		foreach($list as $key =>$value){
    			$items[$value['cms_key']]=$value['cms_value'];
    		}
       	return $items;
    }

    /**
     *
     * @param
     * @return array
     */
    public function InfoTease(){
      	$request = Request::instance();
      	$userid = (new Token())->GetUserid();
      	$type = $request->post('type');
      	$skip = $request->post('skip');
      	$where['userid']=$userid;
      	$where['type']=$type;
		$list=Db::table('cms_tease')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->limit($skip,1)
				->order('sort asc')
				->select();
       	return $list;
    }


    public function InsertInformation(){
    	$request 		= 	Request::instance();
    	$userid			=	$request->post('userid');
        $email 			= 	$request->post('email');
        $signature 		= 	$request->post('signature');
        $mobilephone 	= 	$request->post('mobilephone');
        $college 		= 	$request->post('college');
        $workyears 		= 	$request->post('workyears');
        $intention 		= 	$request->post('intention');
        $specialty		=	$request->post('specialty');
        $record 		=	$request->post('record');
        $headimgurl     =   $request->post('headimgurl');
        $data = array(
            'email'       		=>$email,
            'signature'         =>$signature,
            'mobilephone'       =>$mobilephone,
            'college'          	=>$college,
            'workyears'         =>$workyears,
            'intention'         =>$intention,
            'specialty'         =>$specialty,
            'record'     		=>$record,
            'headimgurl'		=>$headimgurl
         );
        Db::table('cms_user')->where("id=".$userid)->update($data);
        $info=Db::table('cms_user')->where("id=".$userid)->find();
        return $info;
    }
}