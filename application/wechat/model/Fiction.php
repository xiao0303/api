<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Fiction extends BaseModel
{
	/**
     * 列表
     * @param
     * @return array
     */
    public function FictionList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$pagesize = $request->post('pagesize') ? $request->post('pagesize') : 5;
		$page     = $request->post('page') ? $request->post('page') : 0;
		$skip     = ($page-1)*$pagesize>0 ? ($page-1)*$pagesize : 0;
		$keyword = $request->post('keyword') ? $request->post('keyword') : null;
		if(!empty($keyword)){
			$where['chapter_name']=array('like','%'.$keyword.'%');
		}
		$where['type']=1;
		$list=  Db::table('cms_article')
				->where($where)
				->fetchSql(false)
				->cache(false)
				->order(array('id'=>'asc'))
				->limit($skip,$pagesize)
				->select();

		$data['count']=Db::table('cms_article')
				->where($where)
				->count();
		$data['pagesize']=$pagesize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }
    
    public function FictionDetail(){
    	$request = Request::instance();
        $id = $request->post('id');
        $where['id']=$id;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->find();
        return $list;
    }

    public function  FictionDetailLeft(){
        $request = Request::instance();
        $id = $request->post('id');
        $type=$request->post('type');
        $where['id']=array('lt',$id);
        $where['type']=$type;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->order(array('id'=>'desc'))->limit(1)->select();
        $data=$list[0];
        return $data;
    }

    public function  FictionDetailRight(){
    	$request = Request::instance();
        $id = $request->post('id');
        $type=$request->post('type');
        $where['id']=array('gt',$id);
        $where['type']=$type;
        $list=Db::table('cms_article')->fetchSql(false)->cache(true)->where($where)->order(array('id'=>'asc'))->limit(1)->select();
        $data=$list[0];
        return $data;
    }
}