<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Log extends BaseModel
{
	/**
     * 列表
     * @param
     * @return array
     */
    public function LogList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$pagesize = $request->post('pagesize') ? $request->post('pagesize') : 20;
		$page     = $request->post('page') ? $request->post('page') : 0;
		$skip     = ($page-1)*$pagesize>0 ? ($page-1)*$pagesize : 0;

		$list=  Db::table('cms_log')
				->fetchSql(false)
				->cache(false)
				->order(array('id'=>'desc'))
				->limit($skip,$pagesize)
				->select();
		
		$data['count']=Db::table('cms_log')

				->where($where)
				->count();
		$data['pagesize']=$pagesize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }
    
    
    public static function DeleteLog(){
		$request 	= 	Request::instance();
        $id			=	$request->post('id');
        if(is_array($id)){
            $where['id']=array('in',$id);
        }else{
            $where['id']=$id;
        }
        $result=Db::table('cms_log')->fetchSql(false)->where($where)->delete();
        return $result;
	}
}