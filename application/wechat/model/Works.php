<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
use app\wechat\model\Token;
class Works extends BaseModel
{
	/**
     * 列表
     * @param
     * @return array
     */
    public function WorksList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$userid = (new Token())->GetUserid();
		$pageSize = $request->post('pageSize') ? $request->post('pageSize') : 20;
		$pageNumber = $request->post('pageNumber') ? $request->post('pageNumber') : 1;
		$skip     = ($pageNumber)*$pageSize>0 ? ($pageNumber-1)*$pageSize : 0;
		$where['userid']=$userid;
		$where['hidden']=0;
		$list=Db::table('cms_works')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->order(array('sort'=>'asc'))
				->limit($skip,$pagesize)
				->select();
		$data['count']=Db::table('cms_works')->where($where)->count();
		$data['pageSize']=$pageSize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }

    /**
     * 列表
     * @param
     * @return array
     */
    public function WorksDetail(){
      	$request = Request::instance();
      	$userid = (new Token())->GetUserid();
		$id = $request->post('id');
        $where['userid']=$userid;
        $where['id']=$id;
		$list=Db::table('cms_works')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->find();
       	return $list;
    }
}