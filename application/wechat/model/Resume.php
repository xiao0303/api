<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Resume extends BaseModel
{
	/**
     * 获取权限
     * @param
     * @return array
     */
    public function ResumeInfo(){
      	$request = Request::instance();
      	$userid = $request->post('userid');
      	$type = $request->post('type');
      	$id	=  $request->post('id');
		if($id){
            $where['id']=$id;
        }
        if($userid){
            $where['userid']=$userid;
        }
        if($type){
            $where['type']=$type;
        }
       	$data=Db::table('cms_message')->fetchSql(false)->cache(false)->where($where)->find();
       	return $data;
    }
}