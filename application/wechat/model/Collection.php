<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Collection extends BaseModel
{
	/**
     * 列表
     * @param
     * @return array
     */
    public function CollectionList(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$pagesize = $request->post('pagesize') ? $request->post('pagesize') : 5;
		$page     = $request->post('page') ? $request->post('page') : 0;
		$skip     = ($page-1)*$pagesize>0 ? ($page-1)*$pagesize : 0;
		$keyword = $request->post('keyword') ? $request->post('keyword') : null;
		if(!empty($keyword)){
			$where['vocabulary']=array('like','%'.$keyword.'%');
		}
		$where['status']=1;
		$list=  Db::table('cms_collection')
				->where($where)
				->fetchSql(false)
				->cache(false)
				->order(array('editdate'=>'desc'))
				->limit($skip,$pagesize)
				->select();

		$data['count']=Db::table('cms_collection')
				->where($where)
				->count();
		$data['pagesize']=$pagesize;
		$data['list']=$list;
		$data['pageNumber']=$pageNumber;
       	return $data;
    }
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function InsertCollection(){
    	$request 	= 	Request::instance();
        $pronunciation 	= 	$request->post('pronunciation');
        $vocabulary 		= 	$request->post('vocabulary');
        $phrase 		= 	$request->post('phrase');

        $data = array(
            'pronunciation'       	=>$pronunciation,
            'vocabulary'         	=>$vocabulary,
            'phrase'        		=>$phrase,
            'editor'				=>'jerry xiao',
            'editdate'				=>date('Y-m-d H:i:s'),
            'creator'				=>'jerry xiao',
            'createdate'			=>date('Y-m-d H:i:s'),
            'status'				=>1,
         );
        $id=Db::table('cms_collection')->insertGetId($data);
        $info=Db::table('cms_collection')->where("id=".$id)->find();
        return $info;
    }
    
    /**
     * 
     * @param
     * @return array
     */
    public function UpdateCollection(){
    	$request 	= 	Request::instance();
    	$id			=	$request->post('id');
    	$request 	= 	Request::instance();
        $pronunciation 	= 	$request->post('pronunciation');
        $vocabulary 		= 	$request->post('vocabulary');
        $phrase 		= 	$request->post('phrase');

        $data = array(
            'pronunciation'       	=>$pronunciation,
            'vocabulary'         	=>$vocabulary,
            'phrase'        		=>$phrase,
            'editor'				=>'jerry xiao',
            'editdate'				=>date('Y-m-d H:i:s'),
         );
        Db::table('cms_collection')->where("id=".$id)->update($data);
        $info=Db::table('cms_collection')->where("id=".$id)->find();
        return $info;
    }
    
    public static function DeleteCollection(){
		$request 	= 	Request::instance();
		$id			=	$request->post('id');
		$where['id']=is_array($id) ? array('in',$id) : $id;
		$data=[
			'status'=>2
		];
        $result=Db::table('cms_collection')->fetchSql(false)->where("id=".$id)->update($data);
        return $result;
	}
}