<?php
namespace app\wechat\model;
use think\Controller;
use think\Model;
use think\Db;
use think\File;
class BaseModel extends Model
{
	
	/*
	   * 单张图片上传
	   * $name为表单上传的name值
	   * $filePath为为保存在入口文件夹public下面uploads/下面的文件夹名称，没有的话会自动创建
	   * $isthumb 是否上传缩略图 1上传  不填和填写其他为不上传
	   * $width指定缩略宽度
	   * $height指定缩略高度
	   * 自动生成的缩略图保存在$filePath文件夹下面的thumb文件夹里，自动创建
	   * @return array 一个是图片路径，一个是缩略图路径，如下：
	   * array(4) {
		  ["img"] => string(58) "/uploads/pic/20180628\7db8ef69811066ceaa9372db3c7f2fe0.jpg"
		  ["thumb_img"] => string(64) "/uploads/pic/thumb/20180628/7db8ef69811066ceaa9372db3c7f2fe0.jpg"
		  ["msg"] => string(13) "上传成功!"
		  ["status"] => int(1)
		}
	   */
	  protected function uploadOneFile($name,$filePath,$isthumb=0,$width=200,$height=200){	
	    $file = request()->file($name);
	    if($file){
	      $filePaths = ROOT_PATH . 'public' . DS . 'uploads' . DS .$filePath;
	      if(!file_exists($filePaths)){
	        	mkdir($filePaths,0777,true);
	      }
	      $info = $file->move($filePaths);
	      if($info){
	        $imgpath = 'uploads/'.$filePath.'/'.$info->getSaveName();
	        $data['img'] = '/'.$imgpath;
	        if($isthumb==1){
	        	$image = \think\Image::open($imgpath);
		        $date_path = 'uploads/'.$filePath.'/thumb/'.date('Ymd');
		        if(!file_exists($date_path)){
		          	mkdir($date_path,0777,true);
		        }
		        $thumb_path = $date_path.'/'.$info->getFilename();
		        $image->thumb($width, $height)->save($thumb_path);
		        $data['thumb_img'] =  '/'.$thumb_path;
	        }
	        $data['msg']="上传成功!";
	        $data['status']=1;
	        $data['fileurl']=config('File_url');
	        return $data;
	      }else{
	      	$data['msg']=$file->getError();
	        $data['status']=0;
	        return $data;
	      }
	    }
	  }
	/*
       * 上传多张图片 
	   * $name为表单上传的name值
	   * $filePath为为保存在入口文件夹public下面uploads/下面的文件夹名称，没有的话会自动创建
	   * $isthumb 是否上传缩略图 1上传  不填和填写其他为不上传
	   * $width指定缩略宽度
	   * $height指定缩略高度
	   * 自动生成的缩略图保存在$filePath文件夹下面的thumb文件夹里，自动创建
	   * @return array 一个是图片路径，一个是缩略图路径，如下：
	   * array(3) {
			  ["msg"] => string(13) "上传成功!"
			  ["status"] => int(1)
			  ["group"] => array(3) {
			    [0] => array(2) {
			      ["img"] => string(62) "/uploads/Picture/20180628\86af8b2366e1fa98b1a13bf8ad66ff81.jpg"
			      ["thumb_img"] => string(68) "/uploads/Picture/thumb/20180628/86af8b2366e1fa98b1a13bf8ad66ff81.jpg"
			    }
			    [1] => array(2) {
			      ["img"] => string(62) "/uploads/Picture/20180628\e1caba4428f5d9eb20692e63f3e525d4.jpg"
			      ["thumb_img"] => string(68) "/uploads/Picture/thumb/20180628/e1caba4428f5d9eb20692e63f3e525d4.jpg"
			    }
			    [2] => array(2) {
			      ["img"] => string(62) "/uploads/Picture/20180628\52a3ac5d596ae0b9b205b320aff235d3.jpg"
			      ["thumb_img"] => string(68) "/uploads/Picture/thumb/20180628/52a3ac5d596ae0b9b205b320aff235d3.jpg"
			    }
			  }
			}
	 */
    protected function uploadAllFile($name,$filePath,$isthumb=0,$width=200,$height=200)
	{	
	    $files = request()->file($name);
	    foreach($files as $key => $file){
		    if($file){
		      $filePaths = ROOT_PATH . 'public' . DS . 'uploads' . DS .$filePath;
		      if(!file_exists($filePaths)){
		        	mkdir($filePaths,0777,true);
		      }
		      $info = $file->move($filePaths);
		      if($info){
		      	$data['msg']="上传成功!";
	        	$data['status']=1;
	        	$data['fileurl']=config('File_url');
		        $imgpath = 'uploads/'.$filePath.'/'.$info->getSaveName();
		        $data['group'][$key]['img'] = '/'.$imgpath;
		        if($isthumb==1){
			        $image = \think\Image::open($imgpath);
			        $date_path = 'uploads/'.$filePath.'/thumb/'.date('Ymd');
			        if(!file_exists($date_path)){
			          	mkdir($date_path,0777,true);
			        }
			        $thumb_path = $date_path.'/'.$info->getFilename();
			        $image->thumb($width, $height)->save($thumb_path);
			        $data['group'][$key]['thumb_img'] =  '/'.$thumb_path;
		        }
		      }else{
		      	$data['msg']=$file->getError();
	        	$data['status']=0;
		      }
		    }
	    }
	    return $data;
	}

}