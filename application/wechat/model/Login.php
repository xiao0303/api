<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Login extends BaseModel
{
    /*
        账号密码登录
     */
    public static function  GetLogin(){
        $request = Request::instance();
        $loginid = $request->post('loginid');
        $loginpwd = $request->post('loginpwd');
        $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
        if(preg_match("/^1[345678]{1}\d{9}$/",$loginid)){
		    $info =Db::table('cms_user')->where('mobilephone="' . $loginid . '"')->find();
		}elseif(preg_match($pattern,$loginid)){
			$info =Db::table('cms_user')->where('email="' . $loginid . '"')->find();
		}else{
			$info =Db::table('cms_user')->where('username="' . $loginid . '"')->find();
		}
        if ($info !== null) {
            if ($info['loginpwd'] === md5(sha1($loginpwd))) {
				$random=createNonceStr(16);
                $data = array(
                    'lastlogin'     => date("Y-m-d H:i:s",time()),
                    'access_token'  => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
                    'refresh_token' => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
                    'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+60*60*24*7)
                );
                $res=Db::table('cms_user')->where("id=".$info['id'])->update($data);
                if($res){
                	$userlist=Db::table('cms_user')
            			   ->where("id=".$info['id'])
            			   ->field('access_token as token,refresh_token,overdue_date')
            			   ->find();
            		return $userlist;
                }else{
                	return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }
    /*
        刷新refreshtoken
     */
    public static function GetRefreshLogin(){
        $request = Request::instance();
        $refresh_token = $request->post('refresh_token');
        $info =Db::table('cms_user')->where('refresh_token="' . $refresh_token . '"')->find();
        if ($info) {
				$random=createNonceStr(16);
                $data = array(
                    'lastlogin'     => date("Y-m-d H:i:s",time()),
                    'access_token'  => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
                    'refresh_token' => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
                    'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+60*60*24*7),
                    'loginid'       => $info['loginid']
                );
                $res=Db::table('cms_user')->where("id=".$info['id'])->update($data);
                if($res){
                	$userlist=Db::table('cms_user')
            			   ->where("id=".$info['id'])
            			   ->field('lastlogin,access_token as token,refresh_token,overdue_date,loginid')
            			   ->find();
            		return $userlist;
                }else{
                	return false;
                }
        } else {
            return false;
        }
    }


    /*
        	小程序  微信授权登录
     */
    public static function WxOauthLogin($infoUser){
        $loginid = $infoUser['openId'];
        $username = $infoUser['nickName'];
        $headimgurl = $infoUser['avatarUrl'];
        $country = $infoUser['country'];
        $province = $infoUser['province'];
        $city = $infoUser['city'];
        $sex = $infoUser['gender'];
		$random=createNonceStr(16);
        $data = array(
            'loginid'           =>$loginid,
            'username'          =>$username ? $username : '',
            'headimgurl'        =>$headimgurl  ? $headimgurl : '',
            'country'           =>$country   ? $country : '',
            'province'          =>$province   ? $province : '',
            'city'              =>$city   ? $city : '',
            'sex'               =>$sex   ? $sex : '',
            'lastlogin'     => date("Y-m-d H:i:s",time()),
			'access_token'  => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
			'refresh_token' => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
            'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+60*60*24*7)
         );
        $info =Db::table('cms_user')->where('loginid="' . $loginid . '"')->find();
        if ($info !== null) {
        		$data['editdate']=date('Y-m-d H:i:s');
            	$data['editor']=$username ? $username : '';
        		$data1=unset_empty_array($data);
                Db::table('cms_user')
                	->where("id=".$info['id'])
                	->update($data1);
				$userlist=Db::table('cms_user')
							->where("id=".$info['id'])
							->field('lastlogin,access_token as token,refresh_token,overdue_date,loginid')
							->find();
                return $userlist;
        }else{
            $data['usercode']=randusercode();
            $data['sharecode']=randshare();
            $data['createdate']=date('Y-m-d H:i:s');
            $data['creator']=$username ? $username : '';
            $data['editdate']=date('Y-m-d H:i:s');
            $data['editor']=$username ? $username : '';
            $data1=unset_empty_array($data);
            $id=Db::table('cms_user')
            		->insertGetId($data1);
            $userlist=Db::table('cms_user')
            			   ->where("id=".$id)
            			   ->field('lastlogin,access_token as token,refresh_token,overdue_date,loginid')
            			   ->find();
            return $userlist;
        }
    }

    /*
        公众号微信授权登录
     */
    public static function WxSnsLogin($info){
        $loginid = $info['openid'];
        $username = $info['nickname'];
        $headimgurl = $info['headimgurl'];
        $country = $info['country'];
        $province = $info['province'];
        $city = $info['city'];
        $sex = $info['sex'];
        $unionid = $info['unionid'];
		$random=createNonceStr(16);
        $data = array(
            'loginid'           =>$loginid,
            'unionid'           =>$unionid ? $unionid : '',
            'username'          =>$username ? $username : '',
            'headimgurl'        =>$headimgurl  ? $headimgurl : '',
            'country'           =>$country   ? $country : '',
            'province'          =>$province   ? $province : '',
            'city'              =>$city   ? $city : '',
            'sex'               =>$sex   ? $sex : '',
            'lastlogin'     => date("Y-m-d H:i:s",time()),
			'access_token'  => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
			'refresh_token' => base64_encode(md5($info['id'])).base64_encode(md5($random)).base64_encode(md5(time().rand(1000,9999))),
            'overdue_date'  => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+60*60*24*7)
         );
        $info =Db::table('cms_user')->where('loginid="' . $loginid . '"')->find();
        if ($info !== null) {
                $data['editdate']=date('Y-m-d H:i:s');
                $data['editor']=$username ? $username : '';
                $data1=unset_empty_array($data);
                Db::table('cms_user')
                    ->where("id=".$info['id'])
                    ->update($data1);
                $userlist=Db::table('cms_user')
                            ->where("id=".$info['id'])
                            ->field('lastlogin,access_token as token,refresh_token,overdue_date,loginid')
                            ->find();
                return $userlist;
        }else{
            $data['usercode']=randusercode();
            $data['sharecode']=randshare();
            $data['createdate']=date('Y-m-d H:i:s');
            $data['creator']=$username ? $username : '';
            $data['editdate']=date('Y-m-d H:i:s');
            $data['editor']=$username ? $username : '';
            $data1=unset_empty_array($data);
            $id=Db::table('cms_user')
                    ->insertGetId($data1);
            $userlist=Db::table('cms_user')
                           ->where("id=".$id)
                           ->field('lastlogin,access_token as token,refresh_token,overdue_date,loginid')
                           ->find();
            return $userlist;
        }
    }
}
