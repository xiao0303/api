<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use app\wechat\model\Token;
use think\Db;
use think\Request;
class Signin extends BaseModel
{
    
    /**
     * 列表
     * @param
     * @return array
     */ 
    public function SignIn(){
    	$request 	= 	Request::instance();
        $userid 		=	(new Token())->GetUserid();
        $where['userid']	=	$userid;
        $sign=Db::table('cms_sign_log')
        ->field('id,userid,continual,signdate,creator,createdate')
        ->where($where)
        ->fetchSql(false)
        ->whereTime('signdate', 'between', [date("Y-m-d 00:00:00",strtotime("-1 day")), date('Y-m-d 00:00:00',strtotime("+1 day"))])
        ->order('signdate desc')
        ->select();
		if($sign[0]['signdate']>date('Y-m-d 00:00:00')){
			$data['msg']="已签到!";
			$data['sign']=true;
		}else{
			$data['msg']="无签到!";
			$data['sign']=false;
		}
        if(empty($sign)){
        	$data['continual']=0;
        }else{
        	$data['continual']=$sign[0]['continual'];
        }
        return $data;
    }
    
    /**
     * 添加
     * @param
     * @return array
     */
    public function InsertSignIn(){
    	$request 	= 	Request::instance();
        $userid 		=	$request->post('userid');
        $user=Db::table('cms_user')->where("id=".$userid)->find();
        $remark 		=	$request->post('remark');
        
        $continual=Db::table('cms_sign_log')
        ->fetchSql(false)
        ->where(array('userid'=>$userid))
        ->whereTime('signdate', 'between', [date("Y-m-d 00:00:00",strtotime("-1 day")), date('Y-m-d 00:00:00')])
        ->value('continual');
        $continual = $continual==0 ?  1 : $continual+1;
        $where['userid']	=	$userid;
        $where['signdate']	=	array('egt',date('Y-m-d 00:00:00'));
        $issign=Db::table('cms_sign_log')
        ->field('id,userid,continual,signdate,creator,createdate')
        ->fetchSql(false)
        ->where($where)
        ->find();

        if(!empty($issign)){
        	$data=[
        		'msg' =>'签到成功!',
        		'sign'=>true,
        		'continual'=>$issign['continual']
        	];
        	return $data;
        }
        
        $param = array(
            'userid'  			=>$userid,
            'signdate'			=>date('Y-m-d H:i:s'),
            'continual'			=>$continual,
            'remark'			=>$remark ? $remark : '',
            'creator'			=>$user['username'],
            'createdate'		=>date('Y-m-d H:i:s'),
            'editor'			=>$user['username'],
            'editdate'			=>date('Y-m-d H:i:s'),
         );
        $id=Db::table('cms_sign_log')->insertGetId($param);
        $info=Db::table('cms_sign_log')->field('id,userid,continual,signdate,creator,createdate')->where("id=".$id)->find();
        if(!empty($info)){
        	$data=[
        		'msg' =>'签到成功!',
        		'sign'=>true,
        		'continual'=>$info['continual']
        	];
        	return $data;
        }
        
    }
	
	public function Record(){
		$request 	= 	Request::instance();
        $userid 		=	$request->post('userid');
        $where['userid']	=	$userid;
        $record=Db::table('cms_sign_log')->where($where)->column('signdate');
        if(empty($record)){
        	$data=[
        		'msg' =>'无签到记录!',
        		'sign'=>false,
        		'record'=>[]
        	];
        }else{
        	foreach($record as $val){
        		$record1[]=strtotime($val);
        	}
        	$data=[
        		'msg' =>'已签到!',
        		'sign'=>true,
        		'record'=>$record1
        	];
        }
        return $data;
	}
}