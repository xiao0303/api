<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Wechat extends BaseModel
{
	/**
     * 部分
     * @param
     * @return array
     */
    public function WechatCompanyInfo(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$companyid = $request->param('companyid');
		$where['companyid']=$companyid;
		$list=Db::table('cms_wechat')
				->field('id,companyid,appid,url,token,original_id,is_certification,type,redirect_uri,authorization')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->find();
		
       	return $list;
    }

	/**
     * 全部
     * @param
     * @return array
     */
    public function WechatCompanyAllInfo(){
    	$data=array();
		$where=array();
      	$request = Request::instance();
      	$companyid = $request->param('companyid');
		$where['companyid']=$companyid;
		$list=Db::table('cms_wechat')
				->fetchSql(false)
				->cache(false)
				->where($where)
				->find();
		
       	return $list;
    }

}