<?php
namespace app\wechat\model;
use app\wechat\model\BaseModel;
use think\Db;
use think\Request;
class Token extends BaseModel
{
    public function GetUserid(){
        $request = Request::instance();
		$token 	= $request->header('Access-Token');
        $where['access_token']=$token;
        $list=Db::table('cms_user')
        		->field('id as userid')
        		->where($where)
        		->find();
        return $list['userid'];
    }
}