<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/30 0030
 * Time: 12:18
 */

namespace app\wechat\lib\exception;
use app\wechat\lib\BaseException;

class TokenException extends BaseException
{
    //错误具体信息
    public $msg = 'Toekn错误!';
    //自定义错误码
    public $errorCode = 30000;
}