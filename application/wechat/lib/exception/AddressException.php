<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/30 0030
 * Time: 12:18
 */

namespace app\wechat\lib\exception;
use app\wechat\lib\BaseException;

class AddressException extends BaseException
{
    //错误具体信息
    public $msg = '操作错误!';
    //自定义错误码
    public $errorCode = 40000;
}