<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/29 0029
 * Time: 16:58
 */
namespace app\wechat\lib;
use think\Exception;

class BaseException extends Exception
{

    //错误具体信息
    public $msg = '参数错误';
    //自定义错误码
    public $errorCode = 10000;

    public function __construct($params = []){
		parent::__construct();
        if (!is_array($params)){
        	throw new Exception("参数必须是数组!");
        }

        if (array_key_exists('msg', $params)) {
            $this->msg = $params['msg'];
        }
        if (array_key_exists('errorCode', $params)) {
            $this->errorCode = $params['errorCode'];
        }

    }
}