<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Collection as CollectionModel;
use app\wechat\validate\Collection as CollectionValidate;
use app\wechat\lib\exception\ParameterException;

class Collection extends Base
{

	public function __construct(){
		parent::__construct();

	}
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Collection/GetCollectionList
     */

    public function GetCollectionList()
    {
    	$validate = new CollectionValidate();
    	$validate->goCheck('Collection');
        $result=(new CollectionModel())->CollectionList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Collection/GetInsertCollection
     */

    public function GetInsertCollection()
    {
    	$validate = new CollectionValidate();
    	$validate->goCheck('add');
        $result=(new CollectionModel())->InsertCollection();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'新增失败!','errorCode'=>30506));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Collection/GetUpdateCollection
     */

    public function GetUpdateCollection()
    {
    	$validate = new CollectionValidate();
    	$validate->goCheck('edit');
        $result=(new CollectionModel())->UpdateCollection();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'修改失败!','errorCode'=>30507));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Collection/GetDeleteCollection
     */

    public function GetDeleteCollection()
    {
    	$validate = new CollectionValidate();
        $validate->goCheck('del');
        $result=(new CollectionModel())->DeleteCollection();
        if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'删除失败!','errorCode'=>30508));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array(),'删除成功!');
    }
}