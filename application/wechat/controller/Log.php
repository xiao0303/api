<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Log as LogModel;
use app\wechat\validate\Log as LogValidate;
use app\wechat\lib\exception\ParameterException;

class Log extends Base
{

	public function __construct(){
		parent::__construct();

	}
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Log/GetLogList
     */

    public function GetLogList()
    {
    	$validate = new LogValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Log');
        $result=(new LogModel())->LogList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30516));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }




    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Log/GetDeleteLog
     */

    public function GetDeleteLog()
    {
    	$validate = new LogValidate();
		$validate->TokenCheck();
		$validate->SignCheck();
        $validate->goCheck('Del');
        $result=(new LogModel())->DeleteLog();
        if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'删除失败!','errorCode'=>30517));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array(),'删除成功!');
    }
}