<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Userinfo as UserinfoModel;
use app\wechat\validate\Userinfo as UserinfoValidate;
use app\wechat\lib\exception\ParameterException;
use app\wechat\service\Wxuser;

class Userinfo extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Userinfo/GetInformation
     */

    public function GetInformation()
    {
		$validate = new UserinfoValidate();
		$validate->TokenCheck();
        $validate->SignCheck();

        $result=(new UserinfoModel())->Information();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'返回错误!','errorCode'=>30705));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Userinfo/GetInfoQuotations
     */

    public function GetInfoQuotations()
    {
		$validate = new UserinfoValidate();
		$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Quotations');
        $result=(new UserinfoModel())->InfoQuotations();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'返回错误!','errorCode'=>30705));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Userinfo/GetInfoTease
     */

    public function GetInfoTease()
    {
        $result=(new UserinfoModel())->InfoTease();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'返回错误!','errorCode'=>30706));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Userinfo/InsertInformation
     */

    public function InsertInformation()
    {
		$validate = new UserinfoValidate();
		$validate->TokenCheck();
//      $validate->SignCheck();
    	$validate->goCheck('Insert');
        $result=(new UserinfoModel())->InsertInformation();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'返回错误!','errorCode'=>30708));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }



    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Userinfo/WxShare
     */
    public function WxShare(){
        $Wx = new Wxuser();
        $result=$Wx->getSignPackage();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'返回错误!','errorCode'=>30709));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }
}