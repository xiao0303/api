<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Login as LoginModel;
use app\wechat\validate\Login as LoginValidate;
use app\wechat\lib\exception\LoginException;
use app\wechat\service\Wxuser;
use think\Request;
class Login extends Base
{
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Login/oauth
     */

    public function oauth()
    {
    	$validate = new LoginValidate();
    	$validate->goCheck('Login');
        $result=(new LoginModel())->GetLogin();
		if(empty($result)){
        	try{
				throw new LoginException(array('msg'=>'账号密码错误!','errorCode'=>10501));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Login/RefreshOauth
     */

    public function RefreshOauth()
    {
        $validate = new LoginValidate();
        $validate->TokenCheck();
    	$validate->goCheck('Refresh');
        $result=(new LoginModel())->GetRefreshLogin();
		if(empty($result)){
        	try{
				throw new LoginException(array('msg'=>'refresh_token出现异常!','errorCode'=>10502));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array('data'=>$result,'msg'=>'token刷新成功!'));

    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Login/WxOauthLogin
     */

    public function WxOauthLogin()
    {
    	$validate = new LoginValidate();
    	$validate->goCheck('WxOauthLogin');
        $result=(new LoginModel())->WxOauthLogin();
		if(empty($result)){
        	try{
				throw new LoginException(array('msg'=>'授权登录异常!','errorCode'=>10503));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array('data'=>$result,'msg'=>'登录成功!'));
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Login/WxLogin
     */
    public function WxLogin()
    {
        $Wx = new Wxuser();
        if(isset($_GET['code'])){
            $url=$_GET['state'];
            $userinfo=$Wx->wei_get_info($_GET['code']);
            $result=(new LoginModel())->WxSnsLogin($userinfo);
            if(empty($result)){
                try{
                    throw new LoginException(array('msg'=>'授权登录异常!','errorCode'=>10509));
                }catch(\Exception $e){
                    $this->show_error($e->msg,$e->errorCode);
                }
            }
            header("Location:".$url."?loginid=".$result['loginid']."&token=".$result['token']);
        }else{
            $Wxurl=config('File_url').'/wechat/api/v1/Login/WxLogin';
            $url=$_GET['url'];
            $Wx->wei_get_acquire_code($Wxurl,'snsapi_userinfo',$url);
        }
    }

    /*
        *登录（调用wx.login获取）
        * @param $code string
        * @param $rawData string
        * @param $signatrue string
        * @param $encryptedData string
        * @param $iv string
        * @return $code 成功码
        * @return $session3rd 第三方3rd_session
        * @return $data 用户数据
    */
        public function WechatXcxLogin(){
            
            $request = Request::instance();
            $code = $request->post('code');
            if(isset($code)){
                $result=(new Wxuser())->WechatXcxLogin($code);
                if(empty($result)){
                    try{
                        throw new LoginException(array('msg'=>'授权登录异常!','errorCode'=>10510));
                    }catch(\Exception $e){
                        $this->show_error($e->msg,$e->errorCode);
                    }
                }
                $userlogininfo=(new LoginModel())->WxOauthLogin($result);
                if(empty($userlogininfo)){
                    try{
                        throw new LoginException(array('msg'=>'用户授权登录失败!','errorCode'=>10512));
                    }catch(\Exception $e){
                        $this->show_error($e->msg,$e->errorCode);
                    }
                }
                $this->show_success($userlogininfo);
            }else{
                try{
                    throw new LoginException(array('msg'=>'没上传code!','errorCode'=>10511));
                }catch(\Exception $e){
                    $this->show_error($e->msg,$e->errorCode);
                }
            }

 }

}
