<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\validate\Address as AdressValidate;
use app\wechat\model\Address as AddressModel;
use app\wechat\lib\exception\AddressException;
class Address extends Base
{
	/*
	 *  获取列表
	 	http://api.com/index.php/wechat/api/v1/Address/GetAdressList
	 * */

	public function GetAdressList(){
		$validate = new AdressValidate();
        $validate->TokenCheck();
        $validate->SignCheck();
        $List=(new AddressModel())->GetAddressList();
        $this->show_success($List);
	}

	/*
	 *  添加
	 	http://api.com/index.php/wechat/api/v1/Address/InsertAddress
	 * */
	public function InsertAddress(){
		$validate = new AdressValidate();
		$validate->TokenCheck();
        $validate->goCheck('add');
        $validate->SignCheck();
        $Address=(new AddressModel())->InsertAddress();
        if(empty($Address)){
        	try{
				throw new AddressException(array('msg'=>'地址添加失败!','errorCode'=>40001));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($Address);
	}

	/*
	 * 	修改
	 	http://api.com/index.php/wechat/api/v1/Address/UpdateAddress
	 * */
	public function UpdateAddress(){
		$validate = new AdressValidate();
		$validate->TokenCheck();
        $validate->goCheck('update');
        $validate->SignCheck();
        $Address=(new AddressModel())->UpdateAddress();
        if(empty($Address)){
        	try{
				throw new AddressException(array('msg'=>'地址修改失败!','errorCode'=>40002));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($Address);
	}

	/*
	 * 	删除
	 	http://api.com/index.php/wechat/api/v1/Address/DeleteAddress
	 * */
	public function DeleteAddress(){
		$validate = new AdressValidate();
		$validate->TokenCheck();
		$validate->SignCheck();
        $validate->goCheck('del');
        $result=(new AddressModel())->DeleteAddress();
        if(empty($result)){
        	try{
				throw new AddressException(array('msg'=>'地址删除失败!','errorCode'=>40003));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array(),'删除成功!');
	}
}
