<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Company as CompanyModel;
use app\wechat\validate\Company as CompanyValidate;
use app\wechat\lib\exception\ParameterException;
use app\wechat\service\Example as ExampleService;
class Company extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Company/GetCompanyInfo
     */

    public function GetCompanyList()
    {
    	$validate = new CompanyValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('list');
        $result=(new CompanyModel())->CompanyList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30601));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Company/GetInsertCompany
     */

    public function GetInsertCompany()
    {
    	$validate = new CompanyValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('add');
        $result=(new CompanyModel())->InsertCompany();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'新增失败!','errorCode'=>30602));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Company/GetUpdateCompany
     */

    public function GetUpdateCompany()
    {
    	$validate = new CompanyValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('edit');
        $result=(new CompanyModel())->UpdateCompany();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'修改失败!','errorCode'=>30603));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Company/GetDeleteCompany
     */

    public function GetDeleteCompany()
    {
    	$validate = new CompanyValidate();
		$validate->TokenCheck();
		$validate->SignCheck();
        $validate->goCheck('del');
        $result=(new CompanyModel())->DeleteCompany();
        if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'删除失败!','errorCode'=>30604));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array(),'删除成功!');
    }



}