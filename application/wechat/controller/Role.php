<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Role as RoleModel;
use app\wechat\validate\Role as RoleValidate;
use app\wechat\lib\exception\ParameterException;

class Role extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Role/GetRole
     */

    public function GetRole()
    {
		$validate = new RoleValidate();
		$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('List');
        $result=(new RoleModel())->GetRole();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空请补充完整!','errorCode'=>60501));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

}