<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Order as OrderModel;
use app\wechat\validate\Order as OrderValidate;
use app\wechat\service\Example as ExampleService;
use app\wechat\lib\exception\ParameterException;
class Order extends Base
{
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Pay/GetPayOrder
     */

    public function GetPayOrder()
    {
        $validate = new OrderValidate();
        $validate->goCheck('Order');
        $result=(new OrderModel())->GetInsertOrder();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'','errorCode'=>80001));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.xiao0303.info/index.php/wechat/Order/JsapiPay
     */
    public function JsapiPay(){
        $validate = new OrderValidate();
		$validate->TokenCheck();
		$validate->goCheck('OrderPay');
		$validate->SignCheck();
        $result['pay']=(new ExampleService())->jspay();
        $result['config'] = getSignPackage();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'','errorCode'=>80002));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }


    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/Order/notify
     */
    public function notify(){
       $result=(new ExampleService())->notify();

    }
}
