<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use think\Request;

class FilePaper extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2019/4/11
     * http://api.com/index.php/wechat/api/v1/FilePaper/GetFile
     */

    public function GetFile()
    {	
		$request = $this->request;
    	$file_path= ROOT_PATH."public/uploads/pic/20190105/天下第九.txt";
		if(file_exists($file_path)){
			$fp= fopen($file_path,"r");
			$str="";
			while(!feof($fp)){
				$str.= fgets($fp);//逐行读取。如果fgets不写length参数，默认是读取1k。
			}
			$str= str_replace("\r\n","<br/>",$str);
			echo $str;
		}
    }

	
}