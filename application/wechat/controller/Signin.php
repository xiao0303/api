<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Signin as SigninModel;
use app\wechat\validate\Signin as SigninValidate;
use app\wechat\lib\exception\ParameterException;

class Signin extends Base
{


	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2 cms_sign_log
     * http://api.com/index.php/wechat/api/v1/Signin/GetSignIn
     */

    public function GetSignIn()
    {
		$validate = new SigninValidate();
		$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('List');
        $result=(new SigninModel())->SignIn();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'暂无签到!','errorCode'=>60801));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2 cms_sign_log
     * http://api.com/index.php/wechat/api/v1/Signin/InsertSignIn
     */

    public function InsertSignIn()
    {
		$validate = new SigninValidate();
		$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Add');
        $result=(new SigninModel())->InsertSignIn();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'签到失败!','errorCode'=>60802));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2 cms_sign_log
     * http://api.com/index.php/wechat/api/v1/Signin/HistoryRecord
     */

    public function HistoryRecord()
    {
		$validate = new SigninValidate();
		$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('History');
        $result=(new SigninModel())->Record();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'暂无签到!','errorCode'=>60801));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }
}