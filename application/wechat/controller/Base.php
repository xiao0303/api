<?php
namespace app\wechat\controller;
use think\Controller;
use think\Request;

class Base extends Controller
{
    public $request;
    public function __construct(){
        parent::__construct();
        header("Content-Type: text/html; charset=utf-8");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With,Sign,Content-Type,Access-Token,Accept");
        header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE');

        $this->request = Request::instance();
    }
    /**
     * 显示错误，并中断程序执行
     * @param string $des 错误描述
     */
    public  function show_error($msg,$errorCode=400) {
        $arr=array(
            'errorCode'  =>      $errorCode,
            'msg'        =>      $msg,
            'status'     =>      'error'
        );
        //返回内容
        content_log($this->request->controller(),$this->request->baseUrl(),$this->request->ip(),$this->request->post(),$arr);
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        exit();
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     */
    public  function show_success($data=array(),$msg='操作成功!',$errorCode=200) {
        $arr=array(
            'errorCode'  =>      $errorCode,
            'data'       =>      $data,
            'msg'		 =>		 $msg,
            'status'     =>      'ok'
        );
        //返回内容
        content_log($this->request->controller(),$this->request->baseUrl(),$this->request->ip(),$this->request->post(),$arr);
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
        exit();
    }

    // miss 路由：处理没有匹配到的路由规则
    public function miss()
    {
        if (Request::instance()->isOptions()) {
            return ;
        } else {
            echo 'jerryxiao的接口';
        }
    }
}
