<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Project as ProjectModel;
use app\wechat\validate\Project as ProjectValidate;
use app\wechat\lib\exception\ParameterException;

class Project extends Base
{

	public function __construct(){
		parent::__construct();

	}
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Project/GetProjectList
     */

    public function GetProjectList()
    {
    	$validate = new ProjectValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Project');
        $result=(new ProjectModel())->ProjectList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Project/GetInsertProject
     */

    public function GetInsertProject()
    {
    	$validate = new ProjectValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('add');
        $result=(new ProjectModel())->InsertProject();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'新增失败!','errorCode'=>30506));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Project/GetUpdateProject
     */

    public function GetUpdateProject()
    {
    	$validate = new ProjectValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('edit');
        $result=(new ProjectModel())->UpdateProject();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'修改失败!','errorCode'=>30507));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Project/GetProjectList
     */

    public function GetDeleteProject()
    {
    	$validate = new ProjectValidate();
		$validate->TokenCheck();
		$validate->SignCheck();
        $validate->goCheck('del');
        $result=(new ProjectModel())->DeleteProject();
        if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'删除失败!','errorCode'=>30508));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success(array(),'删除成功!');
    }
}