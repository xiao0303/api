<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Works as WorksModel;
use app\wechat\validate\Works as WorksValidate;
use app\wechat\lib\exception\ParameterException;

class Works extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Works/GetWorksList
     */

    public function GetWorksList()
    {
    	$validate = new WorksValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Works');
        $result=(new WorksModel())->WorksList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Works/GetWorksDetail
     */

    public function GetWorksDetail()
    {
    	$validate = new WorksValidate();
    	$validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Detail');
        $result=(new WorksModel())->WorksDetail();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }
}