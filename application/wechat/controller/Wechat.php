<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Wechat as WechatModel;
use app\wechat\model\Login as LoginModel;
use app\wechat\validate\Wechat as WechatValidate;
use app\wechat\lib\exception\ParameterException;

class Wechat extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Wechat/GetWechatCompanyInfo
     */

    public function GetWechatCompanyInfo()
    {
        $validate = new WechatValidate();
        $validate->goCheck('Wechat');
        $validate->SignCheck();
        $result=(new WechatModel())->WechatCompanyInfo();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>30505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Wechat/WechatUserInfo
     */
	public function WechatUserInfo(){
        $code=$_GET['code'];
        if(empty($code)){
        	try{
				throw new ParameterException(array('msg'=>'code不能为空!','errorCode'=>30506));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $data=(new WechatModel())->WechatCompanyAllInfo();
        $info=wei_get_info($data['appid'],$data['appsecret'],$code);
        $result=(new LoginModel())->WxSnsLogin($info);
        if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'获取用户信息失败!','errorCode'=>30507));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        header("Location:".$data['url'].'?token='.$result['token'].'&loginid='.$result['loginid']);
    }
}