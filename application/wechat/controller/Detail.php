<?php


namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Detail as DetailModel;
use app\wechat\validate\Detail as DetailValidate;
use think\Request;
class Detail extends Base
{
    //  http://api.com/index.php/wechat/api/v1/ArticleDetail/index
    public function index(){
        echo '123';
    }
    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/ArticleDetail/GetArticleDetail
     */
    public function GetArticleDetail()
    {
        // 输入数据验证
        $validate = new DetailValidate();
		$validate->TokenCheck();
		$validate->goCheck('detail');
		$validate->SignCheck();
        // 返回数据
        $list=DetailModel::DetailInfo();
        $this->show_success($list);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/ArticleDetail/GetDetailLeft
     */
    public function GetDetailLeft(){
        // 输入数据验证
        $validate = new DetailValidate();
		$validate->TokenCheck();
		$validate->goCheck('detailLeft');
		$validate->SignCheck();
        
        // 返回数据
        $list=DetailModel::DetailLeft();
        $this->show_success($list);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/ArticleDetail/GetDetailLeft
     */
    public function GetDetailRight(){
        // 输入数据验证
        $validate = new DetailValidate();
		$validate->TokenCheck();
		$validate->goCheck('detailRight');
		$validate->SignCheck();
        // 返回数据
        $list=DetailModel::DetailRight();
        $this->show_success($list);
    }

}