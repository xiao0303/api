<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Picture as PictureModel;
use app\wechat\validate\Picture as PictureValidate;
use app\wechat\lib\exception\ParameterException;

class Picture extends Base
{

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Picture/InsertPicture
     */

    public function InsertOnePicture()
    {
        $validate = new PictureValidate();
//         $validate->TokenCheck();
        // $validate->SignCheck();
//      $validate->goCheck('add');
        $result=(new PictureModel())->InsertOnePicture();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'数据为空请补充完整!','errorCode'=>60501));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }

    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Picture/InsertAllPicture
     */

    public function InsertAllPicture()
    {
        $validate = new PictureValidate();
        // $validate->TokenCheck();
        // $validate->SignCheck();
        // $validate->goCheck('add');
        $result=(new PictureModel())->InsertAllPicture();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'数据为空请补充完整!','errorCode'=>60502));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }
}