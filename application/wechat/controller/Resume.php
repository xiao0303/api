<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Resume as ResumeModel;
use app\wechat\validate\Resume as ResumeValidate;
use think\Exception;
class Resume extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Resume/GetResumeInfo
     */

    public function GetResumeInfo()
    {
    	$validate = new ResumeValidate();
    	// $validate->TokenCheck();
        $validate->SignCheck();
    	$validate->goCheck('Resume');
        $result=(new ResumeModel())->ResumeInfo();
		if(empty($result)){
        	try{
				throw new Exception(array('msg'=>'返回错误!','errorCode'=>30501));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

}