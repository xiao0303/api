<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\model\Fiction as FictionModel;
use app\wechat\validate\Fiction as FictionValidate;
use app\wechat\lib\exception\ParameterException;

class Fiction extends Base
{

	public function __construct(){
		parent::__construct();

	}
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Fiction/GetFictionList
     */

    public function GetFictionList()
    {
        $validate = new FictionValidate();
        // $validate->goCheck('Fiction');
        // $validate->TokenCheck();
        // $validate->SignCheck();
    	
        $result=(new FictionModel())->FictionList();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>40505));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }

	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Fiction/GetFictionDetail
     */

    public function GetFictionDetail()
    {
    	$validate = new FictionValidate();
    	$validate->goCheck('Detail');
        $result=(new FictionModel())->FictionDetail();
		if(empty($result)){
        	try{
				throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>40506));
			}catch(\Exception $e){
				$this->show_error($e->msg,$e->errorCode);
			}
        }
        $this->show_success($result);
    }


        /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/v1/ArticleDetail/GetFictionDetailLeft
     */
    public function GetFictionDetailLeft()
    {
        $validate = new FictionValidate();
        $validate->goCheck('Detail');
        $result=(new FictionModel())->FictionDetailLeft();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>40507));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);

    }


    /**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/v1/ArticleDetail/GetFictionDetailRight
     */
    public function GetFictionDetailRight(){
        $validate = new FictionValidate();
        $validate->goCheck('Detail');
        $result=(new FictionModel())->FictionDetailRight();
        if(empty($result)){
            try{
                throw new ParameterException(array('msg'=>'数据为空!','errorCode'=>40508));
            }catch(\Exception $e){
                $this->show_error($e->msg,$e->errorCode);
            }
        }
        $this->show_success($result);
    }
}