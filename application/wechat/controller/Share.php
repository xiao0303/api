<?php
namespace app\wechat\controller;
use app\wechat\controller\Base;
use app\wechat\validate\Share as ShareValidate;

class Share extends Base
{
	/**
     * @author jerry xiao
     * @param array $data 返回json
     * @datetime  2018/3/2
     * http://api.com/index.php/wechat/api/v1/Share/GetShareConfig
     */

    public function GetShareConfig()
    {	
		$validate = new ShareValidate();
		$validate->TokenCheck();
		$validate->goCheck('Config');
		$validate->SignCheck();
		$result['config'] = getSignPackage();
		if(empty($result)){
		    try{
		        throw new ParameterException(array('msg'=>'获取分享配置失败!','errorCode'=>80000));
		    }catch(\Exception $e){
		        $this->show_error($e->msg,$e->errorCode);
		    }
		}
		$this->show_success($result);
    }

}