<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Signin extends BaseValidate
{
    //列表参数验证
    protected   $List=array(
            'userid'   => 'require',

    );
    //列表参数验证
    protected   $Add=array(
            'userid'   => 'require',

    );
    //错误语句提示
    protected $message  =   [
        'userid.require'           => 'userid不能为空!',

    ];
	// 历史记录
	protected $History=array(
		'userid'   => 'require',
	);
}