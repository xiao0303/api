<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Resume extends BaseValidate
{
    //列表参数验证
    protected   $Resume=array(
            'userid'   => 'require',
            'id'  => 'require',
            'type'  => 'require'
    );

    //错误语句提示
    protected $message  =   [
        'userid.require'           => 'userid不能为空!',
        'id.require'          	   => 'id不能为空!',
        'type.require'             => 'type不能为空!',
    ];


}