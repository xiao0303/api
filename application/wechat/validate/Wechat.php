<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Wechat extends BaseValidate
{
    //列表参数验证
    protected   $Wechat=array(
        'sign'          =>      'require',
        'sn'            =>      'require',
        'companyid'     =>      'require'
    );
	

    //错误语句提示
    protected $message  =   [
        'sign.require'          => 'sign不能为空!',
        'sn'					=> 'sn不能为空!',
        'companyid'             =>  'companyid不能为空!'
    ];


}