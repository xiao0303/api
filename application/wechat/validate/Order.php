<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Order extends BaseValidate
{
    //列表参数验证
    protected   $OrderPay=array(
            'orderno'   => 'require',
			'url'		 => 'require'
    );


    //错误语句提示
    protected $message  =   [
        'orderno.require'           => 'ordernoe账号不能为空!',
		'url.require'           => 'url账号不能为空!',
    ];

}