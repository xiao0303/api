<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/29 0029
 * Time: 12:01
 */

namespace app\wechat\validate;


class Address extends BaseValidate
{
    protected $add = [
        'userid'        => 	'require|integer',
        'sex'			=> 	'require',
        'username'		=> 	'require',
        'telphone'		=>	'require',
        'province'		=>	'require',
        'district'		=>	'require',
        'address'		=>	'require'
    ];
    protected $update = [
    	'id'			=>	'require|integer',
        'userid'        => 	'require|integer',
        'sex'			=> 	'require',
        'username'		=> 	'require',
        'telphone'		=>	'require',
        'province'		=>	'require',
        'district'		=>	'require',
        'address'		=>	'require'
    ];
    protected $del     = [
    	'id'			=>	'require|integer',
    	'userid'		=>	'require|integer'
    ];
    
    protected $message=[
    	'id'			=>	'id不能为空!',
        'userid'        => 	'userid不能为空且为整数!',
        'sex'			=> 	'sex不能为空!',
        'username'		=>	'username不能为空!',
        'telphone'		=>	'telphone不能为空!',
        'province'		=>	'province不能为空!',
        'district'		=>	'district不能为空!',
        'address'		=>	'address不能为空!'
    ];

}