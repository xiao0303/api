<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Collection extends BaseValidate
{
    //列表参数验证
    protected   $Collection=array(

    );
	
	protected  $add=array(
		    'pronunciation'      =>	'require',
            'vocabulary'         =>	'require',
            'phrase'        	 =>	'require',
	);
	
	protected  $edit=array(
			'id'				=>	'require|integer',
		    'pronunciation'      =>	'require',
            'vocabulary'         =>	'require',
            'phrase'        	 =>	'require',
	);
	
	protected $del=array(
			'id'				=>	'require|integer',
	);
	
    //错误语句提示
    protected $message  =   [
        'pronunciation.require'		=> '发音不能为空!',
        'vocabulary.require'		=> '词汇不能为空!',
        'phrase.require'			=> '短句不能为空!',
        'id.require'				=> 'id不能为空!',
    ];

	
}