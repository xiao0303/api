<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Project extends BaseValidate
{
    //列表参数验证
    protected   $Project=array(
        
    );
	
	protected  $add=array(
	    'projectname'       =>	'require',
            'startdate'         =>	'require',
            'enddate'        	=>	'require',
            'team'          	=>	'require',
            'companyid'         =>	'require|integer',
            'describe'          =>	'require',
            'tag'     			=>	'require',
            'userid'  			=>	'require|integer',
            'duties'			=>	'require',
	);
	
	protected  $edit=array(
			'id'				=>	'require|integer',
		    'projectname'       =>	'require',
            'startdate'         =>	'require',
            'enddate'        	=>	'require',
            'team'          	=>	'require',
            'companyid'         =>	'require|integer',
            'describe'          =>	'require',
            'tag'     			=>	'require',
            'userid'  			=>	'require|integer',
            'duties'			=>	'require',
	);
	
	protected $del=array(
			'id'				=>	'require|integer',
			'userid'  			=>	'require|integer',
	);
	
    //错误语句提示
    protected $message  =   [
        'projectname.require'		=> 'projectname 不能为空!',
        'startdate.require'			=> 'startdate 不能为空!',
        'enddate.require'			=> 'enddate 不能为空!',
        'team.require'				=> 'team 不能为空!',
        'companyid.require'			=> 'companyid 不能为空!',
        'describe.require'			=> 'describe 不能为空!',
        'tag.require'				=> 'tag 不能为空!',
        'userid.require'           	=> 'userid 不能为空!',
    ];

	
}