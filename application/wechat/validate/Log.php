<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Log extends BaseValidate
{
    //列表参数验证
    protected   $Log=array(

    );

    protected  $Del = [
        'id'				=>	'require|integer',
    ];

    //错误语句提示
    protected $message  =   [
        'id.require'		=> 'id不能为空!',
        'id.integer'		=> 'id为整数!'
    ];

    

}