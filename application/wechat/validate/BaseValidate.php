<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/29 0029
 * Time: 14:08
 */
namespace app\wechat\validate;
use app\wechat\controller\Base as BaseController;
use app\wechat\lib\exception\ParameterException;
use app\wechat\lib\exception\SignException;
use app\wechat\lib\exception\TokenException;
use app\wechat\lib\exception\UserException;
use think\Exception;
use think\Request;
use think\Validate;
use think\Db;

class BaseValidate extends Validate
{
	private $request;
	public function __construct(){
		parent::__construct();
		header("Content-Type: text/html; charset=utf-8");
		$this->request = Request::instance();
	}

    public function goCheck($rule){
        //对参数做校验
        $parames = $this->request->param();
				
        // $result = $this->batch()->check($parames,$this->$rule);
        $result = $this->check($parames,$this->$rule);
        
        if (empty($result)) {
        	try{
				throw new ParameterException(array('msg'=>$this->error));
			}catch(\Exception $e){
                (new BaseController)->show_error($e->msg,$e->errorCode);
			}
        } else {
            return true;
        }
    }

	//验证签名
    public  function SignCheck(){
        //服务端生成签名与客户端验证
        $sign = $this->request->header('Sign');
        if(empty($sign)){
        	try{
				throw new SignException(array('msg'=>'sign不能为空!','errorCode'=>20001));
			}catch(\Exception $e){
				(new BaseController)->show_error($e->msg,$e->errorCode);
			}
        }
		//参数
        $parames = $this->request->param();
        $servicesign=$this->servicesign($parames);

        if($sign!=$servicesign){
        	try{
				throw new SignException();
			}catch(\Exception $e){
				(new BaseController)->show_error($e->msg,$e->errorCode);
			}
        }else{
        	return true;
        }
    }
    //验证access_token
    public function TokenCheck(){
    	$token 	= $this->request->header('Access-Token');
        if(empty($token)){
            try{
				throw new TokenException(array('msg'=>'token不能为空!','errorCode'=>30001));
			}catch(\Exception $e){
               (new BaseController)->show_error($e->msg,$e->errorCode);

			}
        }

        $info=Db::table('cms_user')
        	->cache(true)
        	->where(array(
        				'access_token'=>$token,
        				'overdue_date'=>
        							array('gt',date('Y-m-d H:i:s'))
        				)
        			)
        	->fetchSql(false)
        	->field('id as userid,access_token as token,overdue_date,refresh_token')
        	->find();
        if(empty($info)){
            try{
				throw new TokenException(array('msg'=>'token已过有效期!','errorCode'=>30002));
			}catch(\Exception $e){
				(new BaseController)->show_error($e->msg,$e->errorCode);
			}
        }
    }
    //获取签名
    // 必填参数序列化按照key=value&key=value+key后md5加密转换小写。注：参数值为空不加入签名序列
    protected function servicesign($data=array()){
    	unset($data['sign']);
    	unset($data['token']);
        $str='';
        foreach($data as $k=>$v){
            if($v===null||$v===''){
                unset($data[$k]);
            }
        }
        ksort($data);
        foreach($data as $key =>$value){
            $str.=$key.'='.$value.'&';
        }
        $sign=$str.config("ACCESS");
        $sign=strtolower(MD5($sign));
        return $sign;
     }

}