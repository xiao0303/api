<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Userinfo extends BaseValidate
{
    //列表参数验证


	protected $Insert=array(
			'email'			=>'require|email',
			'signature'		=>'require',
			'mobilephone'	=>'require',
			'college'		=>'require',
			'workyears'		=>'require',
			'intention'		=>'require',
			'specialty'		=>'require',
			'record'		=>'require',
	);
	protected $Quotations=array(
			
	);
    //错误语句提示
    protected $message  =   [
        'userid.require'           		=> 'userid不能为空!',
        'email.require'           		=> 'email不能为空!',
        'email.email'           		=> 'email格式不正确!',
        'signature.require'           	=> 'signature不能为空!',
        'mobilephone.require'           => 'mobilephone不能为空!',
        'college.require'           	=> 'college不能为空!',
        'workyears.require'           	=> 'workyears不能为空!',
        'intention.require'           	=> 'intention不能为空!',
        'specialty.require'           	=> 'specialty不能为空!',
        'record.require'           		=> 'record不能为空!',
    ];


}