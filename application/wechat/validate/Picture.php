<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Picture extends BaseValidate
{
    //列表参数验证
    protected   $List=array(
            'usercode'   => 'require',

    );

    //列表参数验证
    protected   $Add=array(
        'usercode'   => 'require',

    );

    //错误语句提示
    protected $message  =   [
        'usercode.require'           => 'usercode账号不能为空!',

    ];

}