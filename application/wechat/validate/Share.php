<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Share extends BaseValidate
{
    //列表参数验证
    protected   $Config=array(
            'url'   => 'require',

    );

    //错误语句提示
    protected $message  =   [
        'url.require'           => 'url不能为空!',

    ];

}