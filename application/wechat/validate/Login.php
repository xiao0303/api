<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Login extends BaseValidate
{
    //列表参数验证
    protected   $Login=array(
            'loginid'   => 'require',
            'loginpwd'  => 'require',
    );

    //错误语句提示
    protected $message  =   [
        'loginid.require'           => 'loginid账号不能为空!',
        'loginpwd.integer'          => 'loginpwd密码不能为空!',
        'refresh_token'             => 'refresh_token不能为空!',
    ];

    protected $Refresh=array(
        'refresh_token'   => 'require',
        );

    protected $WxOauthLogin=array(
        'openid'          => 'require',
    );
}