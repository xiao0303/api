<?php
namespace app\wechat\validate;

class Detail extends BaseValidate
{
    //列表参数验证
    protected   $detail=array(
            'sn'        => 'require',
            'id'        => 'require|integer',
	    'type'	=> 'require'
    );

    //错误语句提示
    protected $message  =   [
        'sn.require'                => 'SN不能为空!',
        'id.require'                => 'id不能为空!',
        'id.integer'                => '页码必须为整数!',
	'type.require'		    => 'type不能为空!'
    ];

    //添加参数验证
    protected   $articleadd=array(
            'id'          =>  'require|integer',

    );

    protected $detailLeft=array(
            'sn'        => 'require',
            'id'        => 'require|integer',
	    'type'	=> 'require'
        );

    protected $detailRight=array(
            'sn'        => 'require',
            'id'        => 'require|integer',
	    'type'	=> 'require'
        );
}
