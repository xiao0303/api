<?php
namespace app\wechat\validate;
use app\wechat\validate\BaseValidate;

class Company extends BaseValidate
{
    //列表参数验证
    protected   $list=array(
        
    );

	protected  $add=array(
            'companyname'       =>	'require',
            'startdate'         =>	'require',
            'enddate'        	=>	'require',
            'position'          =>	'require',
            'industry'         	=>	'require',
            'total'          	=>	'require',
            'department'        =>	'require',
            'nature'     		=>	'require',
            'workdescribe'     	=>	'require',
            'isfulltime'     	=>	'require',
            'dimissionreason'   =>	'require',
            'important'     	=>	'require',
            'userid'  			=>	'require',
	);
	
	protected  $edit=array(
			'id'				=>	'require|integer',
            'companyname'       =>	'require',
            'startdate'         =>	'require',
            'enddate'        	=>	'require',
            'position'          =>	'require',
            'industry'         	=>	'require',
            'total'          	=>	'require|integer',
            'department'        =>	'require',
            'nature'     		=>	'require',
            'workdescribe'     	=>	'require',
            'isfulltime'     	=>	'require',
            'dimissionreason'   =>	'require',
            'important'     	=>	'require',
            'userid'  			=>	'require|integer',
	);
	
	protected $del=array(
			'id'				=>	'require|integer',
			'userid'  			=>	'require|integer',
	);
	
    //错误语句提示
    protected $message  =   [
        'userid.require'            =>  'userid 不能为空!',
        'companyname.require'       =>	'companyname 不能为空!',
        'startdate.require'         =>	'startdate 不能为空!',
        'enddate.require'        	=>	'enddate 不能为空!',
        'position.require'          =>	'position 不能为空!',
        'industry.require'         	=>	'industry 不能为空!',
        'total.require'          	=>	'total 不能为空!',
        'department.require'        =>	'department 不能为空!',
        'nature.require'     		=>	'nature 不能为空!',
        'workdescribe.require'     	=>	'workdescribe 不能为空!',
        'isfulltime.require'     	=>	'isfulltime 不能为空!',
        'dimissionreason.require'   =>	'dimissionreason 不能为空!',
        'important.require'     	=>	'important 不能为空!',
        'id.integer'     			=>	'id 必须为int类型!',
        'userid.integer'     		=>	'userid 必须为int类型!',
        'total.integer'				=>  'total 必须为int类型!',
    ];


}