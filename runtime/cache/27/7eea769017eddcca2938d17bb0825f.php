<?php
//000000000000
 exit();?>
a:4:{i:0;a:8:{s:2:"id";i:302;s:7:"message";s:10085:"但是在英文和汉字混合的情况下会出现如下问题： <br><br>如果有这样一个字符串 <br>$str="这是一个字符串"; <br>为了截取该串的前10个字符，使用 <br>if(strlen($str)&gt;10) $str=substr($str,10)."…"; <br>那么，echo $str的输出应该是"这是一个字…" <br><br>假设 <br>$str="这是1个字符串"； <br>这个串中包含了一个半角字符，同样执行： <br>if(strlen($str)&gt;10) $str=substr($str,10); <br>由于原字符串$str的第10、11个字符构成了汉字“符”； <br>执行串分割后会将该汉字一分为二，这样被截取的串就会发现乱码现象。 <br><br><br>请问这种问题如何解决？即要使过长字符串实现分割，又不能让它发生乱码？<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="67954" class="copybut" id="copybut67954" onclick="doCopy('code67954')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code67954">
<br>&lt;?php <br>//村里有很多，这个是gb2312 <br>function substrs($content,$length='30') <br>{ <br>    if($length &amp;&amp; strlen($content)&gt;$length) <br>    { <br>        $num=0; <br>        for($i=0;$i&lt;$length-3;$i++) <br>        { <br>            if(ord($content[$i])&gt;127) <br>            { <br>                $num++; <br>            } <br>        } <br>        $num%2==1 ? $content=substr($content,0,$length-4):$content=substr($content,0,$length-3); <br>    } <br>    return $content; <br>} <br>?&gt;<br>
</div> <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="49391" class="copybut" id="copybut49391" onclick="doCopy('code49391')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code49391">
<br>function cutstr($string, $length, $dot = ' ...') { <br>        $strcut = ''; <br>        for($i = 0; $i &lt; $length - strlen($dot) - 1; $i++) { <br>                $strcut .= ord($string[$i]) &gt; 127 ? $string[$i].$string[++$i] : $string[$i]; <br>        } <br>        return $strcut.$dot; <br>}<br>
</div>
<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="31638" class="copybut" id="copybut31638" onclick="doCopy('code31638')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code31638">
<br>function cutTitle($str, $len, $tail = ""){ <br>        $length                = strlen($str); <br>        $lentail        = strlen($tail); <br>        $result                = ""; <br>        if($length &gt; $len){ <br>        $len = $len - $lentail; <br>                for($i = 0;$i &lt; $len;$i ++){ <br>                        if(ord($str[$i]) &lt; 127){ <br>                                $result .= $str[$i]; <br>                        }else{ <br>                                $result .= $str[$i]; <br>                                ++ $i; <br>                                $result .= $str[$i]; <br>                        } <br>                } <br>                $result = strlen($result) &gt; $len ? substr($result, 0, -2) . $tail : $result . $tail; <br>        }else{ <br>                $result = $str; <br>        } <br>        return $result; <br>}<br>
</div> <br><strong>以下是一些补充：<br></strong>1. 截取GB2312中文字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="58341" class="copybut" id="copybut58341" onclick="doCopy('code58341')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code58341"> <br>&lt;?php <br>//截取中文字符串 <br>function mysubstr($str, $start, $len) { <br>$tmpstr = ""; <br>$strlen = $start + $len; <br>for($i = 0; $i &lt; $strlen; $i++) { <br>if(ord(substr($str, $i, 1)) &gt; 0xa0) { <br>$tmpstr .= substr($str, $i, 2); <br>$i++; <br>} else <br>$tmpstr .= substr($str, $i, 1); <br>} <br>return $tmpstr; <br>} <br>?&gt; <br>
</div> <br>2. 截取utf8编码的多字节字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="78231" class="copybut" id="copybut78231" onclick="doCopy('code78231')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code78231"> <br>&lt;?php <br>//截取utf8字符串 <br>function utf8Substr($str, $from, $len) <br>{ <br>return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'. <br>'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s', <br>'$1',$str); <br>} <br>?&gt; <br>
</div> <br>3. UTF-8、GB2312都支持的汉字截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="45119" class="copybut" id="copybut45119" onclick="doCopy('code45119')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code45119"> <br>&lt;?php <br>/* <br>Utf-8、gb2312都支持的汉字截取函数 <br>cut_str(字符串, 截取长度, 开始长度, 编码); <br>编码默认为 utf-8 <br>开始长度默认为 0 <br>*/function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') <br>{ <br>if($code == 'UTF-8') <br>{ <br>$pa ="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/"; <br>preg_match_all($pa, $string, $t_string); if(count($t_string[0]) - $start &gt; $sublen) return join('', array_slice($t_string[0], $start, $sublen))."..."; <br>return join('', array_slice($t_string[0], $start, $sublen)); <br>} <br>else <br>{ <br>$start = $start*2; <br>$sublen = $sublen*2; <br>$strlen = strlen($string); <br>$tmpstr = ''; for($i=0; $i&lt;$strlen; $i++) <br>{ <br>if($i&gt;=$start &amp;&amp; $i&lt;($start+$sublen)) <br>{ <br>if(ord(substr($string, $i, 1))&gt;129) <br>{ <br>$tmpstr.= substr($string, $i, 2); <br>} <br>else <br>{ <br>$tmpstr.= substr($string, $i, 1); <br>} <br>} <br>if(ord(substr($string, $i, 1))&gt;129) $i++; <br>} <br>if(strlen($tmpstr)&lt;$strlen ) $tmpstr.= "..."; <br>return $tmpstr; <br>} <br>}$str = "abcd需要截取的字符串"; <br>echo cut_str($str, 8, 0, 'gb2312'); <br>?&gt; <br>
</div> <br>4. BugFree 的字符截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="44455" class="copybut" id="copybut44455" onclick="doCopy('code44455')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code44455"> <br>&lt;?php <br>/** <br>* @package BugFree <br>* @version $Id: FunctionsMain.inc.php,v 1.32 2005/09/24 11:38:37 wwccss Exp $ <br>* <br>* <br>* Return part of a string(Enhance the function substr()) <br>* <br>* @author Chunsheng Wang <br>* @param string $String the string to cut. <br>* @param int $Length the length of returned string. <br>* @param booble $Append whether append "...": false|true <br>* @return string the cutted string. <br>*/ <br>function sysSubStr($String,$Length,$Append = false) <br>{ <br>if (strlen($String) &lt;= $Length ) <br>{ <br>return $String; <br>} <br>else <br>{ <br>$I = 0; <br>while ($I &lt; $Length) <br>{ <br>$StringTMP = substr($String,$I,1); <br>if ( ord($StringTMP) &gt;=224 ) <br>{ <br>$StringTMP = substr($String,$I,3); <br>$I = $I + 3; <br>} <br>elseif( ord($StringTMP) &gt;=192 ) <br>{ <br>$StringTMP = substr($String,$I,2); <br>$I = $I + 2; <br>} <br>else <br>{ <br>$I = $I + 1; <br>} <br>$StringLast[] = $StringTMP; <br>} <br>$StringLast = implode("",$StringLast); <br>if($Append) <br>{ <br>$StringLast .= "..."; <br>} <br>return $StringLast; <br>} <br>}$String = "www.baidu.com"; <br>$Length = "18"; <br>$Append = false; <br>echo sysSubStr($String,$Length,$Append); <br>?&gt; <br>
</div>
            
            <div class="art_xg">
<h4>您可能感兴趣的文章:</h4>
<ul>
<li><a href="/article/9387.htm" title="PHP字符转义相关函数小结(php下的转义字符串)" target="_blank">PHP字符转义相关函数小结(php下的转义字符串)</a></li>
<li><a href="/article/23755.htm" title="php strstr查找字符串中是否包含某些字符的查找函数" target="_blank">php strstr查找字符串中是否包含某些字符的查找函数</a></li>
<li><a href="/article/27743.htm" title="PHP通过iconv将字符串从GBK转换为UTF8字符集" target="_blank">PHP通过iconv将字符串从GBK转换为UTF8字符集</a></li>
<li><a href="/article/28864.htm" title="PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数" target="_blank">PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数</a></li>
<li><a href="/article/29844.htm" title="PHP中将字符串转化为整数(int) intval() printf() 性能测试" target="_blank">PHP中将字符串转化为整数(int) intval() printf() 性能测试</a></li>
<li><a href="/article/30321.htm" title="PHP中去掉字符串首尾空格的方法" target="_blank">PHP中去掉字符串首尾空格的方法</a></li>
<li><a href="/article/30529.htm" title="PHP 查找字符串常用函数介绍" target="_blank">PHP 查找字符串常用函数介绍</a></li>
<li><a href="/article/35077.htm" title="PHP 数组和字符串互相转换实现方法" target="_blank">PHP 数组和字符串互相转换实现方法</a></li>
<li><a href="/article/46458.htm" title="PHP preg_replace() 正则替换所有符合条件的字符串" target="_blank">PHP preg_replace() 正则替换所有符合条件的字符串</a></li>
<li><a href="/article/115885.htm" title="详解PHP处理字符串类似indexof的方法函数" target="_blank">详解PHP处理字符串类似indexof的方法函数</a></li>
</ul>
</div>";s:7:"creator";s:5:"jerry";s:10:"createdate";s:19:"2018-03-01 12:10:35";s:10:"novel_name";s:24:"php字符串截取问题";s:12:"original_url";s:36:"http://www.jb51.net/article/4999.htm";s:12:"chapter_name";s:24:"php字符串截取问题";s:4:"type";i:2;}i:1;a:8:{s:2:"id";i:303;s:7:"message";s:10085:"但是在英文和汉字混合的情况下会出现如下问题： <br><br>如果有这样一个字符串 <br>$str="这是一个字符串"; <br>为了截取该串的前10个字符，使用 <br>if(strlen($str)&gt;10) $str=substr($str,10)."…"; <br>那么，echo $str的输出应该是"这是一个字…" <br><br>假设 <br>$str="这是1个字符串"； <br>这个串中包含了一个半角字符，同样执行： <br>if(strlen($str)&gt;10) $str=substr($str,10); <br>由于原字符串$str的第10、11个字符构成了汉字“符”； <br>执行串分割后会将该汉字一分为二，这样被截取的串就会发现乱码现象。 <br><br><br>请问这种问题如何解决？即要使过长字符串实现分割，又不能让它发生乱码？<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="67954" class="copybut" id="copybut67954" onclick="doCopy('code67954')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code67954">
<br>&lt;?php <br>//村里有很多，这个是gb2312 <br>function substrs($content,$length='30') <br>{ <br>    if($length &amp;&amp; strlen($content)&gt;$length) <br>    { <br>        $num=0; <br>        for($i=0;$i&lt;$length-3;$i++) <br>        { <br>            if(ord($content[$i])&gt;127) <br>            { <br>                $num++; <br>            } <br>        } <br>        $num%2==1 ? $content=substr($content,0,$length-4):$content=substr($content,0,$length-3); <br>    } <br>    return $content; <br>} <br>?&gt;<br>
</div> <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="49391" class="copybut" id="copybut49391" onclick="doCopy('code49391')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code49391">
<br>function cutstr($string, $length, $dot = ' ...') { <br>        $strcut = ''; <br>        for($i = 0; $i &lt; $length - strlen($dot) - 1; $i++) { <br>                $strcut .= ord($string[$i]) &gt; 127 ? $string[$i].$string[++$i] : $string[$i]; <br>        } <br>        return $strcut.$dot; <br>}<br>
</div>
<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="31638" class="copybut" id="copybut31638" onclick="doCopy('code31638')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code31638">
<br>function cutTitle($str, $len, $tail = ""){ <br>        $length                = strlen($str); <br>        $lentail        = strlen($tail); <br>        $result                = ""; <br>        if($length &gt; $len){ <br>        $len = $len - $lentail; <br>                for($i = 0;$i &lt; $len;$i ++){ <br>                        if(ord($str[$i]) &lt; 127){ <br>                                $result .= $str[$i]; <br>                        }else{ <br>                                $result .= $str[$i]; <br>                                ++ $i; <br>                                $result .= $str[$i]; <br>                        } <br>                } <br>                $result = strlen($result) &gt; $len ? substr($result, 0, -2) . $tail : $result . $tail; <br>        }else{ <br>                $result = $str; <br>        } <br>        return $result; <br>}<br>
</div> <br><strong>以下是一些补充：<br></strong>1. 截取GB2312中文字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="58341" class="copybut" id="copybut58341" onclick="doCopy('code58341')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code58341"> <br>&lt;?php <br>//截取中文字符串 <br>function mysubstr($str, $start, $len) { <br>$tmpstr = ""; <br>$strlen = $start + $len; <br>for($i = 0; $i &lt; $strlen; $i++) { <br>if(ord(substr($str, $i, 1)) &gt; 0xa0) { <br>$tmpstr .= substr($str, $i, 2); <br>$i++; <br>} else <br>$tmpstr .= substr($str, $i, 1); <br>} <br>return $tmpstr; <br>} <br>?&gt; <br>
</div> <br>2. 截取utf8编码的多字节字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="78231" class="copybut" id="copybut78231" onclick="doCopy('code78231')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code78231"> <br>&lt;?php <br>//截取utf8字符串 <br>function utf8Substr($str, $from, $len) <br>{ <br>return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'. <br>'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s', <br>'$1',$str); <br>} <br>?&gt; <br>
</div> <br>3. UTF-8、GB2312都支持的汉字截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="45119" class="copybut" id="copybut45119" onclick="doCopy('code45119')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code45119"> <br>&lt;?php <br>/* <br>Utf-8、gb2312都支持的汉字截取函数 <br>cut_str(字符串, 截取长度, 开始长度, 编码); <br>编码默认为 utf-8 <br>开始长度默认为 0 <br>*/function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') <br>{ <br>if($code == 'UTF-8') <br>{ <br>$pa ="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/"; <br>preg_match_all($pa, $string, $t_string); if(count($t_string[0]) - $start &gt; $sublen) return join('', array_slice($t_string[0], $start, $sublen))."..."; <br>return join('', array_slice($t_string[0], $start, $sublen)); <br>} <br>else <br>{ <br>$start = $start*2; <br>$sublen = $sublen*2; <br>$strlen = strlen($string); <br>$tmpstr = ''; for($i=0; $i&lt;$strlen; $i++) <br>{ <br>if($i&gt;=$start &amp;&amp; $i&lt;($start+$sublen)) <br>{ <br>if(ord(substr($string, $i, 1))&gt;129) <br>{ <br>$tmpstr.= substr($string, $i, 2); <br>} <br>else <br>{ <br>$tmpstr.= substr($string, $i, 1); <br>} <br>} <br>if(ord(substr($string, $i, 1))&gt;129) $i++; <br>} <br>if(strlen($tmpstr)&lt;$strlen ) $tmpstr.= "..."; <br>return $tmpstr; <br>} <br>}$str = "abcd需要截取的字符串"; <br>echo cut_str($str, 8, 0, 'gb2312'); <br>?&gt; <br>
</div> <br>4. BugFree 的字符截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="44455" class="copybut" id="copybut44455" onclick="doCopy('code44455')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code44455"> <br>&lt;?php <br>/** <br>* @package BugFree <br>* @version $Id: FunctionsMain.inc.php,v 1.32 2005/09/24 11:38:37 wwccss Exp $ <br>* <br>* <br>* Return part of a string(Enhance the function substr()) <br>* <br>* @author Chunsheng Wang <br>* @param string $String the string to cut. <br>* @param int $Length the length of returned string. <br>* @param booble $Append whether append "...": false|true <br>* @return string the cutted string. <br>*/ <br>function sysSubStr($String,$Length,$Append = false) <br>{ <br>if (strlen($String) &lt;= $Length ) <br>{ <br>return $String; <br>} <br>else <br>{ <br>$I = 0; <br>while ($I &lt; $Length) <br>{ <br>$StringTMP = substr($String,$I,1); <br>if ( ord($StringTMP) &gt;=224 ) <br>{ <br>$StringTMP = substr($String,$I,3); <br>$I = $I + 3; <br>} <br>elseif( ord($StringTMP) &gt;=192 ) <br>{ <br>$StringTMP = substr($String,$I,2); <br>$I = $I + 2; <br>} <br>else <br>{ <br>$I = $I + 1; <br>} <br>$StringLast[] = $StringTMP; <br>} <br>$StringLast = implode("",$StringLast); <br>if($Append) <br>{ <br>$StringLast .= "..."; <br>} <br>return $StringLast; <br>} <br>}$String = "www.baidu.com"; <br>$Length = "18"; <br>$Append = false; <br>echo sysSubStr($String,$Length,$Append); <br>?&gt; <br>
</div>
            
            <div class="art_xg">
<h4>您可能感兴趣的文章:</h4>
<ul>
<li><a href="/article/9387.htm" title="PHP字符转义相关函数小结(php下的转义字符串)" target="_blank">PHP字符转义相关函数小结(php下的转义字符串)</a></li>
<li><a href="/article/23755.htm" title="php strstr查找字符串中是否包含某些字符的查找函数" target="_blank">php strstr查找字符串中是否包含某些字符的查找函数</a></li>
<li><a href="/article/27743.htm" title="PHP通过iconv将字符串从GBK转换为UTF8字符集" target="_blank">PHP通过iconv将字符串从GBK转换为UTF8字符集</a></li>
<li><a href="/article/28864.htm" title="PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数" target="_blank">PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数</a></li>
<li><a href="/article/29844.htm" title="PHP中将字符串转化为整数(int) intval() printf() 性能测试" target="_blank">PHP中将字符串转化为整数(int) intval() printf() 性能测试</a></li>
<li><a href="/article/30321.htm" title="PHP中去掉字符串首尾空格的方法" target="_blank">PHP中去掉字符串首尾空格的方法</a></li>
<li><a href="/article/30529.htm" title="PHP 查找字符串常用函数介绍" target="_blank">PHP 查找字符串常用函数介绍</a></li>
<li><a href="/article/35077.htm" title="PHP 数组和字符串互相转换实现方法" target="_blank">PHP 数组和字符串互相转换实现方法</a></li>
<li><a href="/article/46458.htm" title="PHP preg_replace() 正则替换所有符合条件的字符串" target="_blank">PHP preg_replace() 正则替换所有符合条件的字符串</a></li>
<li><a href="/article/115885.htm" title="详解PHP处理字符串类似indexof的方法函数" target="_blank">详解PHP处理字符串类似indexof的方法函数</a></li>
</ul>
</div>";s:7:"creator";s:5:"jerry";s:10:"createdate";s:19:"2018-03-01 12:10:50";s:10:"novel_name";s:24:"php字符串截取问题";s:12:"original_url";s:36:"http://www.jb51.net/article/4999.htm";s:12:"chapter_name";s:24:"php字符串截取问题";s:4:"type";i:2;}i:2;a:8:{s:2:"id";i:304;s:7:"message";s:10085:"但是在英文和汉字混合的情况下会出现如下问题： <br><br>如果有这样一个字符串 <br>$str="这是一个字符串"; <br>为了截取该串的前10个字符，使用 <br>if(strlen($str)&gt;10) $str=substr($str,10)."…"; <br>那么，echo $str的输出应该是"这是一个字…" <br><br>假设 <br>$str="这是1个字符串"； <br>这个串中包含了一个半角字符，同样执行： <br>if(strlen($str)&gt;10) $str=substr($str,10); <br>由于原字符串$str的第10、11个字符构成了汉字“符”； <br>执行串分割后会将该汉字一分为二，这样被截取的串就会发现乱码现象。 <br><br><br>请问这种问题如何解决？即要使过长字符串实现分割，又不能让它发生乱码？<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="67954" class="copybut" id="copybut67954" onclick="doCopy('code67954')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code67954">
<br>&lt;?php <br>//村里有很多，这个是gb2312 <br>function substrs($content,$length='30') <br>{ <br>    if($length &amp;&amp; strlen($content)&gt;$length) <br>    { <br>        $num=0; <br>        for($i=0;$i&lt;$length-3;$i++) <br>        { <br>            if(ord($content[$i])&gt;127) <br>            { <br>                $num++; <br>            } <br>        } <br>        $num%2==1 ? $content=substr($content,0,$length-4):$content=substr($content,0,$length-3); <br>    } <br>    return $content; <br>} <br>?&gt;<br>
</div> <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="49391" class="copybut" id="copybut49391" onclick="doCopy('code49391')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code49391">
<br>function cutstr($string, $length, $dot = ' ...') { <br>        $strcut = ''; <br>        for($i = 0; $i &lt; $length - strlen($dot) - 1; $i++) { <br>                $strcut .= ord($string[$i]) &gt; 127 ? $string[$i].$string[++$i] : $string[$i]; <br>        } <br>        return $strcut.$dot; <br>}<br>
</div>
<br><div class="codetitle">
<span><a style="CURSOR: pointer" data="31638" class="copybut" id="copybut31638" onclick="doCopy('code31638')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code31638">
<br>function cutTitle($str, $len, $tail = ""){ <br>        $length                = strlen($str); <br>        $lentail        = strlen($tail); <br>        $result                = ""; <br>        if($length &gt; $len){ <br>        $len = $len - $lentail; <br>                for($i = 0;$i &lt; $len;$i ++){ <br>                        if(ord($str[$i]) &lt; 127){ <br>                                $result .= $str[$i]; <br>                        }else{ <br>                                $result .= $str[$i]; <br>                                ++ $i; <br>                                $result .= $str[$i]; <br>                        } <br>                } <br>                $result = strlen($result) &gt; $len ? substr($result, 0, -2) . $tail : $result . $tail; <br>        }else{ <br>                $result = $str; <br>        } <br>        return $result; <br>}<br>
</div> <br><strong>以下是一些补充：<br></strong>1. 截取GB2312中文字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="58341" class="copybut" id="copybut58341" onclick="doCopy('code58341')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code58341"> <br>&lt;?php <br>//截取中文字符串 <br>function mysubstr($str, $start, $len) { <br>$tmpstr = ""; <br>$strlen = $start + $len; <br>for($i = 0; $i &lt; $strlen; $i++) { <br>if(ord(substr($str, $i, 1)) &gt; 0xa0) { <br>$tmpstr .= substr($str, $i, 2); <br>$i++; <br>} else <br>$tmpstr .= substr($str, $i, 1); <br>} <br>return $tmpstr; <br>} <br>?&gt; <br>
</div> <br>2. 截取utf8编码的多字节字符串 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="78231" class="copybut" id="copybut78231" onclick="doCopy('code78231')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code78231"> <br>&lt;?php <br>//截取utf8字符串 <br>function utf8Substr($str, $from, $len) <br>{ <br>return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'. <br>'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s', <br>'$1',$str); <br>} <br>?&gt; <br>
</div> <br>3. UTF-8、GB2312都支持的汉字截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="45119" class="copybut" id="copybut45119" onclick="doCopy('code45119')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code45119"> <br>&lt;?php <br>/* <br>Utf-8、gb2312都支持的汉字截取函数 <br>cut_str(字符串, 截取长度, 开始长度, 编码); <br>编码默认为 utf-8 <br>开始长度默认为 0 <br>*/function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') <br>{ <br>if($code == 'UTF-8') <br>{ <br>$pa ="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/"; <br>preg_match_all($pa, $string, $t_string); if(count($t_string[0]) - $start &gt; $sublen) return join('', array_slice($t_string[0], $start, $sublen))."..."; <br>return join('', array_slice($t_string[0], $start, $sublen)); <br>} <br>else <br>{ <br>$start = $start*2; <br>$sublen = $sublen*2; <br>$strlen = strlen($string); <br>$tmpstr = ''; for($i=0; $i&lt;$strlen; $i++) <br>{ <br>if($i&gt;=$start &amp;&amp; $i&lt;($start+$sublen)) <br>{ <br>if(ord(substr($string, $i, 1))&gt;129) <br>{ <br>$tmpstr.= substr($string, $i, 2); <br>} <br>else <br>{ <br>$tmpstr.= substr($string, $i, 1); <br>} <br>} <br>if(ord(substr($string, $i, 1))&gt;129) $i++; <br>} <br>if(strlen($tmpstr)&lt;$strlen ) $tmpstr.= "..."; <br>return $tmpstr; <br>} <br>}$str = "abcd需要截取的字符串"; <br>echo cut_str($str, 8, 0, 'gb2312'); <br>?&gt; <br>
</div> <br>4. BugFree 的字符截取函数 <br>代码如下: <br><div class="codetitle">
<span><a style="CURSOR: pointer" data="44455" class="copybut" id="copybut44455" onclick="doCopy('code44455')"><u>复制代码</u></a></span> 代码如下:</div>
<div class="codebody" id="code44455"> <br>&lt;?php <br>/** <br>* @package BugFree <br>* @version $Id: FunctionsMain.inc.php,v 1.32 2005/09/24 11:38:37 wwccss Exp $ <br>* <br>* <br>* Return part of a string(Enhance the function substr()) <br>* <br>* @author Chunsheng Wang <br>* @param string $String the string to cut. <br>* @param int $Length the length of returned string. <br>* @param booble $Append whether append "...": false|true <br>* @return string the cutted string. <br>*/ <br>function sysSubStr($String,$Length,$Append = false) <br>{ <br>if (strlen($String) &lt;= $Length ) <br>{ <br>return $String; <br>} <br>else <br>{ <br>$I = 0; <br>while ($I &lt; $Length) <br>{ <br>$StringTMP = substr($String,$I,1); <br>if ( ord($StringTMP) &gt;=224 ) <br>{ <br>$StringTMP = substr($String,$I,3); <br>$I = $I + 3; <br>} <br>elseif( ord($StringTMP) &gt;=192 ) <br>{ <br>$StringTMP = substr($String,$I,2); <br>$I = $I + 2; <br>} <br>else <br>{ <br>$I = $I + 1; <br>} <br>$StringLast[] = $StringTMP; <br>} <br>$StringLast = implode("",$StringLast); <br>if($Append) <br>{ <br>$StringLast .= "..."; <br>} <br>return $StringLast; <br>} <br>}$String = "www.baidu.com"; <br>$Length = "18"; <br>$Append = false; <br>echo sysSubStr($String,$Length,$Append); <br>?&gt; <br>
</div>
            
            <div class="art_xg">
<h4>您可能感兴趣的文章:</h4>
<ul>
<li><a href="/article/9387.htm" title="PHP字符转义相关函数小结(php下的转义字符串)" target="_blank">PHP字符转义相关函数小结(php下的转义字符串)</a></li>
<li><a href="/article/23755.htm" title="php strstr查找字符串中是否包含某些字符的查找函数" target="_blank">php strstr查找字符串中是否包含某些字符的查找函数</a></li>
<li><a href="/article/27743.htm" title="PHP通过iconv将字符串从GBK转换为UTF8字符集" target="_blank">PHP通过iconv将字符串从GBK转换为UTF8字符集</a></li>
<li><a href="/article/28864.htm" title="PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数" target="_blank">PHP中文处理 中文字符串截取(mb_substr)和获取中文字符串字数</a></li>
<li><a href="/article/29844.htm" title="PHP中将字符串转化为整数(int) intval() printf() 性能测试" target="_blank">PHP中将字符串转化为整数(int) intval() printf() 性能测试</a></li>
<li><a href="/article/30321.htm" title="PHP中去掉字符串首尾空格的方法" target="_blank">PHP中去掉字符串首尾空格的方法</a></li>
<li><a href="/article/30529.htm" title="PHP 查找字符串常用函数介绍" target="_blank">PHP 查找字符串常用函数介绍</a></li>
<li><a href="/article/35077.htm" title="PHP 数组和字符串互相转换实现方法" target="_blank">PHP 数组和字符串互相转换实现方法</a></li>
<li><a href="/article/46458.htm" title="PHP preg_replace() 正则替换所有符合条件的字符串" target="_blank">PHP preg_replace() 正则替换所有符合条件的字符串</a></li>
<li><a href="/article/115885.htm" title="详解PHP处理字符串类似indexof的方法函数" target="_blank">详解PHP处理字符串类似indexof的方法函数</a></li>
</ul>
</div>";s:7:"creator";s:5:"jerry";s:10:"createdate";s:19:"2018-03-01 12:12:03";s:10:"novel_name";s:24:"php字符串截取问题";s:12:"original_url";s:36:"http://www.jb51.net/article/4999.htm";s:12:"chapter_name";s:24:"php字符串截取问题";s:4:"type";i:2;}i:3;a:8:{s:2:"id";i:306;s:7:"message";s:4074:"<p>第一种：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile()
{
 $filename=realpath("resume.html"); //文件名
 $date=date("Ymd-H:i:m");
 Header( "Content-type:  application/octet-stream "); 
 Header( "Accept-Ranges:  bytes "); 
Header( "Accept-Length: " .filesize($filename));
 header( "Content-Disposition:  attachment;  filename= {$date}.doc"); 
 echo file_get_contents($filename);
 readfile($filename); 
}
downfile();
?&gt;</pre>
</div>
<p>或<br></p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile($fileurl)
{
 ob_start(); 
 $filename=$fileurl;
 $date=date("Ymd-H:i:m");
 header( "Content-type:  application/octet-stream "); 
 header( "Accept-Ranges:  bytes "); 
 header( "Content-Disposition:  attachment;  filename= {$date}.doc"); 
 $size=readfile($filename); 
  header( "Accept-Length: " .$size);
}
 $url="url地址";
 downfile($url);
?&gt; 
</pre>
</div>
<p>第二种：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile($fileurl)
{
$filename=$fileurl;
$file  =  fopen($filename, "rb"); 
Header( "Content-type:  application/octet-stream "); 
Header( "Accept-Ranges:  bytes "); 
Header( "Content-Disposition:  attachment;  filename= 4.doc"); 
$contents = "";
while (!feof($file)) {
 $contents .= fread($file, 8192);
}
echo $contents;
fclose($file); 
}
$url="url地址";
downfile($url);
?&gt;</pre>
</div>
<p>PHP实现下载文件的两种方法。分享下，有用到的朋友看看哦。</p>
<p>方法一：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php
/**
* 下载文件
* header函数
*
*/
header('Content-Description: File Transfer');

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($filepath));
header('Content-Transfer-Encoding: binary');
header('Expires: 0′);
header('Cache-Control: must-revalidate, post-check=0, pre-check=0′);
header('Pragma: public');
header('Content-Length: ' . filesize($filepath));
readfile($file_path);
?&gt;</pre>
</div>
<p>了解php中header函数的用法。</p>
<p>方法二：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php
//文件下载
//readfile
$fileinfo = pathinfo($filename);
header('Content-type: application/x-'.$fileinfo['extension']);
header('Content-Disposition: attachment; filename='.$fileinfo['basename']);
header('Content-Length: '.filesize($filename));
readfile($thefile);
exit();
?&gt;</pre>
</div>
            
            <div class="art_xg">
<h4>您可能感兴趣的文章:</h4>
<ul>
<li><a href="/article/5090.htm" title="PHP文件下载类" target="_blank">PHP文件下载类</a></li>
<li><a href="/article/27100.htm" title="php中强制下载文件的代码（解决了IE下中文文件名乱码问题）" target="_blank">php中强制下载文件的代码（解决了IE下中文文件名乱码问题）</a></li>
<li><a href="/article/30563.htm" title="Php中文件下载功能实现超详细流程分析" target="_blank">Php中文件下载功能实现超详细流程分析</a></li>
<li><a href="/article/30697.htm" title="php下载文件的代码示例" target="_blank">php下载文件的代码示例</a></li>
<li><a href="/article/44111.htm" title="php实现文件下载(支持中文文名)" target="_blank">php实现文件下载(支持中文文名)</a></li>
<li><a href="/article/53977.htm" title="php实现文件下载代码分享" target="_blank">php实现文件下载代码分享</a></li>
<li><a href="/article/61781.htm" title="跨浏览器PHP下载文件名中的中文乱码问题解决方法" target="_blank">跨浏览器PHP下载文件名中的中文乱码问题解决方法</a></li>
<li><a href="/article/66207.htm" title="PHP实现远程下载文件到本地" target="_blank">PHP实现远程下载文件到本地</a></li>
<li><a href="/article/122420.htm" title="php实现支持中文的文件下载功能示例" target="_blank">php实现支持中文的文件下载功能示例</a></li>
</ul>
</div>";s:7:"creator";s:5:"jerry";s:10:"createdate";s:19:"2018-03-01 12:22:11";s:10:"novel_name";s:58:"php 下载保存文件保存到本地的两种实现方法";s:12:"original_url";s:37:"http://www.jb51.net/article/40485.htm";s:12:"chapter_name";s:58:"php 下载保存文件保存到本地的两种实现方法";s:4:"type";i:2;}}