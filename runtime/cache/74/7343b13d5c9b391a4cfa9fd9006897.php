<?php
//000000000000
 exit();?>
a:8:{s:2:"id";i:306;s:7:"message";s:4074:"<p>第一种：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile()
{
 $filename=realpath("resume.html"); //文件名
 $date=date("Ymd-H:i:m");
 Header( "Content-type:  application/octet-stream "); 
 Header( "Accept-Ranges:  bytes "); 
Header( "Accept-Length: " .filesize($filename));
 header( "Content-Disposition:  attachment;  filename= {$date}.doc"); 
 echo file_get_contents($filename);
 readfile($filename); 
}
downfile();
?&gt;</pre>
</div>
<p>或<br></p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile($fileurl)
{
 ob_start(); 
 $filename=$fileurl;
 $date=date("Ymd-H:i:m");
 header( "Content-type:  application/octet-stream "); 
 header( "Accept-Ranges:  bytes "); 
 header( "Content-Disposition:  attachment;  filename= {$date}.doc"); 
 $size=readfile($filename); 
  header( "Accept-Length: " .$size);
}
 $url="url地址";
 downfile($url);
?&gt; 
</pre>
</div>
<p>第二种：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php 
function downfile($fileurl)
{
$filename=$fileurl;
$file  =  fopen($filename, "rb"); 
Header( "Content-type:  application/octet-stream "); 
Header( "Accept-Ranges:  bytes "); 
Header( "Content-Disposition:  attachment;  filename= 4.doc"); 
$contents = "";
while (!feof($file)) {
 $contents .= fread($file, 8192);
}
echo $contents;
fclose($file); 
}
$url="url地址";
downfile($url);
?&gt;</pre>
</div>
<p>PHP实现下载文件的两种方法。分享下，有用到的朋友看看哦。</p>
<p>方法一：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php
/**
* 下载文件
* header函数
*
*/
header('Content-Description: File Transfer');

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($filepath));
header('Content-Transfer-Encoding: binary');
header('Expires: 0′);
header('Cache-Control: must-revalidate, post-check=0, pre-check=0′);
header('Pragma: public');
header('Content-Length: ' . filesize($filepath));
readfile($file_path);
?&gt;</pre>
</div>
<p>了解php中header函数的用法。</p>
<p>方法二：</p>
<div class="jb51code">
<pre class="brush:php;">
&lt;?php
//文件下载
//readfile
$fileinfo = pathinfo($filename);
header('Content-type: application/x-'.$fileinfo['extension']);
header('Content-Disposition: attachment; filename='.$fileinfo['basename']);
header('Content-Length: '.filesize($filename));
readfile($thefile);
exit();
?&gt;</pre>
</div>
            
            <div class="art_xg">
<h4>您可能感兴趣的文章:</h4>
<ul>
<li><a href="/article/5090.htm" title="PHP文件下载类" target="_blank">PHP文件下载类</a></li>
<li><a href="/article/27100.htm" title="php中强制下载文件的代码（解决了IE下中文文件名乱码问题）" target="_blank">php中强制下载文件的代码（解决了IE下中文文件名乱码问题）</a></li>
<li><a href="/article/30563.htm" title="Php中文件下载功能实现超详细流程分析" target="_blank">Php中文件下载功能实现超详细流程分析</a></li>
<li><a href="/article/30697.htm" title="php下载文件的代码示例" target="_blank">php下载文件的代码示例</a></li>
<li><a href="/article/44111.htm" title="php实现文件下载(支持中文文名)" target="_blank">php实现文件下载(支持中文文名)</a></li>
<li><a href="/article/53977.htm" title="php实现文件下载代码分享" target="_blank">php实现文件下载代码分享</a></li>
<li><a href="/article/61781.htm" title="跨浏览器PHP下载文件名中的中文乱码问题解决方法" target="_blank">跨浏览器PHP下载文件名中的中文乱码问题解决方法</a></li>
<li><a href="/article/66207.htm" title="PHP实现远程下载文件到本地" target="_blank">PHP实现远程下载文件到本地</a></li>
<li><a href="/article/122420.htm" title="php实现支持中文的文件下载功能示例" target="_blank">php实现支持中文的文件下载功能示例</a></li>
</ul>
</div>";s:7:"creator";s:5:"jerry";s:10:"createdate";s:19:"2018-03-01 12:22:11";s:10:"novel_name";s:58:"php 下载保存文件保存到本地的两种实现方法";s:12:"original_url";s:37:"http://www.jb51.net/article/40485.htm";s:12:"chapter_name";s:58:"php 下载保存文件保存到本地的两种实现方法";s:4:"type";i:2;}