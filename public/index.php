<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
// 定义应用目录
define('APP_PATH', __DIR__ . './../application/');

//开启调试
define('APP_DEBUG',True);
// //指的目录
// define('XIAO','http://api.com/static/Home/');
// define('XIAOINFO','http://api.com/index.php');
// 定义配置文件目录和应用目录同级
define('CONF_PATH', __DIR__.'/../config/');
//define('XIAO','http://182.61.14.150:4321/static/Home/');
//define('XIAOINFO','http://182.61.14.150:4321/index.php');
// 加载框架引导文件
require __DIR__ . './../thinkphp/start.php';