<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // 定义资源路由
    '__rest__'=>[
        'wechat'          =>'wechat',
    ],
    // // 【基础】账号密码登录
     'wechat/api/v1/Login/oauth' => ['wechat/Login/oauth', ['method' => 'post|get']],
    // // 【基础】刷新登录
     'wechat/api/v1/Login/RefreshOauth' => ['wechat/Login/RefreshOauth', ['method' => 'post']],
    // // 【基础】微信授权登录
     'wechat/api/v1/Login/WxOauthLogin' => ['wechat/Login/WxOauthLogin', ['method' => 'post']],
    // // 
    // // 【基础】微信小程序授权登录
    'wechat/api/v1/Login/WechatXcxLogin' => ['wechat/Login/WechatXcxLogin', ['method' => 'post']],

    //文章列表
    'wechat/api/v1/Article/GetArticleList' => ['wechat/Article/GetArticleList', ['method' => 'get|post']],
    //文章详情
    'wechat/api/v1/Detail/GetArticleDetail' => ['wechat/Detail/GetArticleDetail', ['method' => 'get|post']],
    'wechat/api/v1/Detail/GetDetailLeft' => ['wechat/Detail/GetDetailLeft', ['method' => 'get|post']],
    'wechat/api/v1/Detail/GetDetailRight' => ['wechat/Detail/GetDetailRight', ['method' => 'get|post']],

      // // 【地址管理】获取列表
     'wechat/api/v1/Address/GetAdressList' => ['wechat/Address/GetAdressList', ['method' => 'post']],
      // // 【地址管理】添加
     'wechat/api/v1/Address/InsertAddress' => ['wechat/Address/InsertAddress', ['method' => 'post']],
      // // 【地址管理】修改
     'wechat/api/v1/Address/UpdateAddress' => ['wechat/Address/UpdateAddress', ['method' => 'post']],
     // // 【地址管理】修改
     'wechat/api/v1/Address/DeleteAddress' => ['wechat/Address/DeleteAddress', ['method' => 'post']],

     // // 【英文管理】获取列表
     'wechat/api/v1/Collection/GetCollectionList' => ['wechat/Collection/GetCollectionList', ['method' => 'post']],
     // // 【英文管理】添加
     'wechat/api/v1/Collection/GetInsertCollection' => ['wechat/Collection/GetInsertCollection', ['method' => 'post']],
    // // 【英文管理】修改
     'wechat/api/v1/Collection/GetUpdateCollection' => ['wechat/Collection/GetUpdateCollection', ['method' => 'post']],
    // // 【英文管理】修改
     'wechat/api/v1/Collection/GetDeleteCollection' => ['wechat/Collection/GetDeleteCollection', ['method' => 'post']],

     // // 【主体管理】获取列表
     'wechat/api/v1/Company/GetCompanyList' => ['wechat/Company/GetCompanyList', ['method' => 'post']],
     // // 【主体管理】添加
     'wechat/api/v1/Company/GetInsertCompany' => ['wechat/Company/GetInsertCompany', ['method' => 'post']],
    // // 【主体管理】修改
     'wechat/api/v1/Company/GetUpdateCompany' => ['wechat/Company/GetUpdateCompany', ['method' => 'post']],
    // // 【主体管理】修改
     'wechat/api/v1/Company/GetDeleteCompany' => ['wechat/Company/GetDeleteCompany', ['method' => 'post']],

     // // 【文章管理】获取列表
     'wechat/api/v1/Fiction/GetFictionList' => ['wechat/Fiction/GetFictionList', ['method' => 'post|get']],
    // // 【文章管理】 文章详情
     'wechat/api/v1/Fiction/GetFictionDetail' => ['wechat/Fiction/GetFictionDetail', ['method' => 'post']],
    // //  【文章管理】 文章详情下一章
     'wechat/v1/ArticleDetail/GetFictionDetailLeft'  => ['wechat/Fiction/GetFictionDetailLeft', ['method' => 'post']],
     // //  【文章管理】 文章详情上一章
     'wechat/v1/ArticleDetail/GetFictionDetailRight'  => ['wechat/Fiction/GetFictionDetailRight', ['method' => 'post']],

     // // 【上传图片】上传单张图片
     'wechat/api/v1/Picture/InsertOnePicture' => ['wechat/Picture/InsertOnePicture', ['method' => 'post']],
     // // 【上传图片】上传多张张图片
     'wechat/api/v1/Picture/InsertAllPicture' => ['wechat/Picture/InsertAllPicture', ['method' => 'post']],

     // // 【项目管理】获取列表
     'wechat/api/v1/Project/GetProjectList' => ['wechat/Project/GetProjectList', ['method' => 'post']],
     // // 【项目管理】添加
     'wechat/api/v1/Project/GetInsertProject' => ['wechat/Project/GetInsertProject', ['method' => 'post']],
    // //  【项目管理】修改
     'wechat/api/v1/Project/GetUpdateProject' => ['wechat/Project/GetUpdateProject', ['method' => 'post']],
    // //  【项目管理】修改
     'wechat/api/v1/Project/GetDeleteProject' => ['wechat/Project/GetDeleteProject', ['method' => 'post']],

     // // 【简历管理】获取列表
     'wechat/api/v1/Resume/GetResumeInfo' => ['wechat/Resume/GetResumeInfo', ['method' => 'post']],

     // // 【签到管理】获取列表
     'wechat/api/v1/Signin/GetSignIn' => ['wechat/Signin/GetSignIn', ['method' => 'post']],
     // // 【签到管理】签到
     'wechat/api/v1/Signin/InsertSignIn' => ['wechat/Signin/InsertSignIn', ['method' => 'post']],
    // //  【签到管理】历史签到
     'wechat/api/v1/Signin/HistoryRecord' => ['wechat/Signin/HistoryRecord', ['method' => 'post']],

     // // 【用户管理】获取单个用户信息
     'wechat/api/v1/Userinfo/GetInformation' => ['wechat/Userinfo/GetInformation', ['method' => 'post']],
     // // 【用户管理】获取自定义参数话语
     'wechat/api/v1/Userinfo/GetInfoQuotations' => ['wechat/Userinfo/GetInfoQuotations', ['method' => 'post']],
    // //  【用户管理】用户撩妹
     'wechat/api/v1/Userinfo/GetInfoTease' => ['wechat/Userinfo/GetInfoTease', ['method' => 'post']],
    // //  【用户管理】获取用户简历信息
     'wechat/api/v1/Userinfo/InsertInformation' => ['wechat/Userinfo/InsertInformation', ['method' => 'post']],

     // // 【工作案列】获取列表
     'wechat/api/v1/Works/GetWorksList' => ['wechat/Works/GetWorksList', ['method' => 'post']],
     // // 【工作案列】添加
     'wechat/api/v1/Works/GetWorksDetail' => ['wechat/Works/GetWorksDetail', ['method' => 'post']],

     // // 【微信支付】jsapi
     'wechat/api/v1/Order/JsapiPay' => ['wechat/Order/JsapiPay', ['method' => 'post|get']],
     // // 微信网页授权登录
     'wechat/api/v1/Login/WxLogin' => ['wechat/Login/WxLogin', ['method' => 'post']],
     // // 微信分享
     'wechat/api/v1/Userinfo/WxShare' => ['wechat/Userinfo/WxShare', ['method' => 'post']],


     // // 微信公众号配置信息
     'wechat/api/v1/Wechat/GetWechatCompanyInfo' => ['wechat/Wechat/GetWechatCompanyInfo', ['method' => 'post']],
     // 获取openid
     'wechat/api/v1/Wechat/WechatUserInfo' => ['wechat/Wechat/WechatUserInfo', ['method' => 'post']],
    //  测试webSocket
    'wechat/api/v1/Webswoole/GetSocket' => ['wechat/Webswoole/GetSocket', ['method' => 'post|get']],
    // 获取log
    'wechat/api/v1/Log/GetLogList' => ['wechat/Log/GetLogList', ['method' => 'post|get']],
    // 删除log
    'wechat/api/v1/Log/GetDeleteLog' => ['wechat/Log/GetDeleteLog', ['method' => 'post|get']],
    // 获取简历信息
    'resume' => ['home/index/index', ['method' => 'post|get']],
    'wechat/api/v1/Resume/GetResumeInfo' => ['wechat/Resume/GetResumeInfo', ['method' => 'post|get']],
	// 获取文件
	'wechat/api/v1/FilePaper/GetFile'  => ['wechat/FilePaper/GetFile' ,['method' => 'post|get']],
	//微信分享
	'wechat/api/v1/Share/GetShareConfig'  => ['wechat/Share/GetShareConfig' ,['method' => 'post|get']],
    // MISS路由
    '__miss__'  => 'wechat/Base/miss',
];

